import Cinema from "Components/Cinema/Cinema";
import CineplexDetail from "Components/Cineplex/CineplexDetail/CineplexDetail";
import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./Components/Authentication/Login/Login";
import Register from "./Components/Authentication/Register/Register";
import Blog from "./Components/Blog/Blog";
import Home from "./Components/Home/Home";
import MovieDetails from "Components/MovieDetails/MovieDetails";
import Cineplex from "Components/Cineplex/Cineplex";
import ListMovies from "Components/ListMovies/ListMovies";
import BookTicket from "Components/BookTicket/BookTicket";
import PageNotFound from "Components/UI/PagesError/PageNotFound/PageNotFound";
import ModalBookingTicket from "Components/Cineplex/ModalBookingTicket/ModalBookingTicket";
import Payment from "Components/Payment/Payment";
import AuthRoute from "Components/PrivateRoute/AuthRoute";
import UserProfile from "Components/UserProfile/UserProfile";
import { ToastContainer } from "react-toastify";
import Config from "config";
import ScrollToTop from "Components/ScrollToTop/ScrollToTop";

export default function App() {
  return (
    <BrowserRouter>
      <ScrollToTop >
        <Routes>
          <Route path="" element={<Home />} />
          <Route path="blog-dien-anh/*" element={<Blog />} />
          <Route path="cinema">
            <Route path="" element={<Cineplex />} />
            <Route path="galaxy/:slug" element={<CineplexDetail />} />
          </Route>
          <Route path="movie/:movieId" element={<MovieDetails />} />
          <Route path="/lich-chieu" element={<ListMovies />} />

          <Route path="booking">
            <Route path='ticket-consession' element={<BookTicket />} />
            <Route path="seats" element={<ModalBookingTicket />} />
            <Route path="payment" element={
              <AuthRoute requiredAuth>
                <Route path="" element={<Payment />} />
              </AuthRoute>
            } />
          </Route>
          <Route path="account">

            <Route path="profile" element={
              <AuthRoute requiredAuth>
                <Route path="" element={<UserProfile />}></Route>
              </AuthRoute>
            } />

            <Route path='login' element={
              <AuthRoute requiredGuest>
                <Route path="" element={<Login />} />
              </AuthRoute>
            } />

            <Route path='register' element={
              <AuthRoute requiredGuest>
                <Route path="" element={<Register />} />
              </AuthRoute>
            } />

          </Route>
          <Route path='*' element={<PageNotFound />} />
        </Routes>
      </ScrollToTop>
      <ToastContainer />
    </BrowserRouter>
  );
}

declare global {
  interface Date {
    addHours: (h: number) => Date;
  }

  interface Date {
    customFormat: (formatString: string) => string;
  }
}

Date.prototype.addHours = function (h: number) {
  this.setTime(this.getTime() + (h * 60 * 60 * 1000));
  return this;
}

Date.prototype.customFormat = function (formatString: any) {
  var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhhh, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
  YY = ((YYYY = this.getFullYear()) + "").slice(-2);
  MM = (M = this.getMonth() + 1) < 10 ? ('0' + M) : M;
  MMM = (MMMM = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][M - 1]).substring(0, 3);
  DD = (D = this.getDate()) < 10 ? ('0' + D) : D;
  DDD = (DDDD = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][this.getDay()]).substring(0, 3);
  th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) === 1) ? 'st' : (dMod === 2) ? 'nd' : (dMod === 3) ? 'rd' : 'th';

  formatString = formatString.replace("#YYYY#", YYYY).replace("#YY#", YY).replace("#MMMM#", MMMM).replace("#MMM#", MMM).replace("#MM#", MM).replace("#M#", M).replace("#DDDD#", DDDD).replace("#DDD#", DDD).replace("#DD#", DD).replace("#D#", D).replace("#th#", th);
  h = (hhh = this.getHours());
  if (h === 0) h = 24;
  if (h > 12) h -= 12;
  hh = h < 10 ? ('0' + h) : h;
  hhhh = hhh < 10 ? ('0' + hhh) : hhh;
  AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
  mm = (m = this.getMinutes()) < 10 ? ('0' + m) : m;
  ss = (s = this.getSeconds()) < 10 ? ('0' + s) : s;
  return formatString.replace("#hhhh#", hhhh).replace("#hhh#", hhh).replace("#hh#", hh).replace("#h#", h).replace("#mm#", mm).replace("#m#", m).replace("#ss#", ss).replace("#s#", s).replace("#ampm#", ampm).replace("#AMPM#", AMPM);
};