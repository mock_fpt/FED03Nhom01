const Config = {
    NumberShowDate: 7,
    durationToast: 1500,
    maxBookingSeats: 20,
    domain: "tuka.trungkenbi.tk",
}

export default Config;