const PATH = "https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO"

const APIs = {
    //==== API: MOVIE ====//
    //Get list all movie: showing and coming soon.
    GET_ALL_MOVIES: PATH+'/cinema/nowAndSoon',
    //Get movie's information by id movie (schedule showing a movie)
    GET_MOVIE_BY_ID: (movieId: string) => {
        return PATH+`/cinema/movie/${movieId}`;
    },


    //==== API: CINEMA ====//
    //Get list all cinema.
    GET_ALL_CINEMAS: PATH+'/cinema/cinemas',
    //Get movie's information by id movie (schedule showing a movie)
    GET_ALL_MOVIE_OF_CINEMA: (code: string) => {
        return PATH+`/cinema/cinemas/${code}`;
    },


    // API: USER
    POST_LOGIN: PATH+"/user/Login",
    POST_REGISTER: PATH+"/user/user",
    CHANGE_PASSWORD: PATH+"/user/ChangePassword",

    //==== API: BOOKING ====//
    //Get list all booking
    GET_BOOKING_LIST: PATH+'/cinema/booking',
    //Get booking details
    GET_BOOKING_DETAILS: PATH+'/cinema/booking/detail',

    GET_CITY: PATH+"/cinema/city",


    //==== API: BANK ====//
    GET_ALL_BANK: PATH+'/Bank/Bank',
    GET_ALL_BANK_CARD: PATH+'/Bank/BankCard',
    CHECK_BANK_CARD: PATH+'/Bank/BankCard',
    UPDATE_BALANCE_BANK_CARD: PATH+'/Bank/BankCard',
    SAVE_BANK_CARD: PATH+"/Bank/CardRef",
    GET_CARD_BY_EMAIL: (email : string) => {
        return PATH+`/Bank/CardRef/${email}`;
    },

    //==== API: TICKET ====//
    GET_ALL_TICKET: PATH+'/cinema/Ticket',
    CREATE_TICKET: PATH+'/cinema/Ticket',
    GET_TICKET_BY_SHOWCODE: (showCode : string) => {
        return PATH+`/cinema/TicketByShowCode/${showCode}`;
    },
    GET_TICKET_BY_EMAIL: (email : string) => {
        return PATH+`/cinema/TicketByEmail/${email}`;
    },

}

export default APIs;