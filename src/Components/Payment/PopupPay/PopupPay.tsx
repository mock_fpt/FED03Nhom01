import React, { useEffect, useState, useRef } from 'react'
import './PopupPay.scss';
import APIs from 'service/APIs';
import { toast, ToastContainer } from 'react-toastify';
import useCookies from 'react-cookie/cjs/useCookies';
import { useNavigate } from 'react-router-dom';
import eConsession from 'Components/Interface/eConsession';
import Format from 'Components/Common/Format';
import Config from 'config';


var CurrencyFormat = require('react-currency-format');


interface eProps {
    setOpenPopup: (isOpenPopup: boolean) => void,
    state: any,
    totalPrice: number
}

interface eCardInfo {
    "BankId": number,
    "CardNumber": string,
    "Email": string,
    "CardName": string,
    "ExpireDate": string,
    "CVV": string,
    "Balance": number,
    "CreateBy": string
}

interface eBank {
    "Id": number,
    "Name": string,
    "Logo": string
}



const initialValues = {
    "bankId": "",
    "num-1": "",
    "num-2": "",
    "num-3": "",
    "num-4": "",
    expiredMonth: "00",
    expiredYear: "00",
    holderName: "CARDHOLDER NAME",
    cvc: ""
};

function PopupPay(props: eProps) {
    const navigate = useNavigate();
    const [cookies, setCookies] = useCookies(['userInfo']);
    const [values, setValues] = useState(initialValues);
    const [banks, setBanks] = useState<Array<eBank>>([]);

    useEffect(() => {
        console.log(values);
    }, [values])

    useEffect(() => {
        fetch(APIs.GET_ALL_BANK)
            .then(res => res.json())
            .then(data => {
                setBanks(data);
            })
    }, [])


    const handleChangeNumberCard = (e: React.KeyboardEvent<HTMLInputElement>) => {
        const { maxLength, value, name } = e.currentTarget;
        const [fieldName, fieldIndex] = name.split("-");
        let fieldIntIndex = parseInt(fieldIndex, 10);

        if (e.key === "Backspace") {
            if (value.length === 0) {
                if (fieldIntIndex > 1) {
                    // Lấy ô input ở trước
                    const prevfield = document.querySelector<HTMLInputElement>(
                        `input[name=${fieldName}-${fieldIntIndex - 1}]`
                    );
                    // Nếu có thì focus đến nó
                    if (prevfield !== null) {
                        prevfield.focus();
                    }
                } else return;
            }
            setValues({
                ...values,
                [name]: value,
            });
        } else if (e.keyCode >= 48 && e.keyCode <= 57) { // Is number

            if (value.length <= maxLength) {
                setValues({
                    ...values,
                    [name]: value,
                });
            }

            // Nếu như lớn hơn giá trị maxLength quy định
            if (value.length >= maxLength) {
                // Và nếu như không ở ô cuối
                if (fieldIntIndex < 4) {
                    // Lấy ô input tiếp theo
                    const nextfield = document.querySelector<HTMLInputElement>(
                        `input[name=${fieldName}-${fieldIntIndex + 1}]`
                    );
                    // Nếu có thì focus đến nó
                    if (nextfield !== null) {
                        nextfield.focus();
                    }
                }
            }
        } else {
            e.currentTarget.value = e.currentTarget.value.slice(0, -1);
        }
    };


    const handleInputChange = (e: React.KeyboardEvent<HTMLInputElement> | React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.currentTarget;
        setValues({
            ...values,
            [name]: value,
        });
    };

    const handlerChangeExpired = (e: React.KeyboardEvent<HTMLInputElement>) => {
        const { maxLength, value, name } = e.currentTarget;
        if ((e.keyCode >= 48 && e.keyCode <= 57) || e.key === "Backspace") {
            setValues({
                ...values,
                [name]: value,
            });
        } else {
            e.currentTarget.value = e.currentTarget.value.slice(0, -1);
        }
    }



    const handlerClickPayNow = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        const bodyCardInfo = {
            "BankId": values.bankId,
            "CardNumber": values['num-1'] + values['num-2'] + values['num-3'] + values['num-4'],
            "CardName": values.holderName.toUpperCase(),
            "ExpireDate": values.expiredMonth + values.expiredYear,
            "CVV": values.cvc
        }

        const bodyCreateTicket = {
            ...bodyCardInfo,
            "Price": props.totalPrice,
            "ShowCode": props.state.session.id,
            "Email": cookies.userInfo.Email,
            "CinemaName": props.state.cineplex.name,
            "TheaterName": props.state.session.screenName,
            "FilmName": props.state.film.name,
            "ImageLandscape": props.state.film.imageLandscape,
            "ImagePortrait": props.state.film.imagePortrait,
            "Combo": Format.getComboString(props.state.lstItemsChossed),
            "SeatCode": props.state.listSelectSeatsString.join(', '),
            "ShowTime": props.state.session.showDate.split("/").reverse().join("-") + 'T' + props.state.session.showTime + 'Z'
        }

        // Check bank card
        fetch(APIs.CHECK_BANK_CARD, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify(bodyCardInfo)
        })
            .then(res => res.json())
            .catch((err) => {
                // Card Incorrect
                toast("Thông tin thẻ không hợp lệ", { type: 'error', autoClose: Config.durationToast })
            })
            .then((cardInfo: eCardInfo) => {
                // Card Correct, check balance
                if (cardInfo.Balance >= props.totalPrice) {
                    // Create Ticket
                    fetch(APIs.CREATE_TICKET, {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json',
                            'accept': 'application/json'
                        },
                        body: JSON.stringify(bodyCreateTicket)
                    })
                        .then(res => {
                            if (res.status === 200) {
                                // SAVE CARD
                                fetch(APIs.SAVE_BANK_CARD, {
                                    method: "POST",
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'accept': 'application/json'
                                    },
                                    body: JSON.stringify({
                                        "CardNumber": cardInfo.CardNumber,
                                        "Email": cookies.userInfo.Email
                                    })
                                });

                                toast(`Chúc mừng bạn đã thanh toán đơn hàng thành công`, { type: 'success', autoClose: Config.durationToast });
                                setTimeout(() => {
                                    navigate('/account/profile');
                                }, 2000);
                            } else {
                                toast(`Có lỗi tạo vé xảy ra. Vui lòng liên hệ hỗ trợ viên để được giải đáp`, { type: 'error', autoClose: Config.durationToast });
                            }
                        })
                } else {
                    toast(`Thẻ của bạn không đủ ${props.totalPrice}đ để thực hiện thanh toán!`, { type: 'warning', autoClose: Config.durationToast })
                }
            })
    }

    // Set countdown timer payment
    let timer: any;
    let timeLeft = 5 * 60 + 1;
    const tick = () => {
        timer = document.querySelector('[data-id=timer]');
        if (timeLeft > 0) {
            timeLeft--;
            const date = new Date('2000-01-01 00:00:00');
            date.setSeconds(timeLeft);
            const str = date.toISOString();
            timer.children[0].innerText = str[14];
            timer.children[1].innerText = str[15];
            timer.children[3].innerText = str[17];
            timer.children[4].innerText = str[18];
        }
        else {
            toast('Hết thời gian thanh toán!', { position: 'top-center', autoClose: 1500, closeButton: false });
            props.setOpenPopup(false);
        }
    }


    // Handler Click Close Popup
    const handlerClickClosePopup = () => {
        let confirm = window.confirm("Bạn có muốn thoát không?" );
        if(confirm) {
            props.setOpenPopup(false);
        }
    }

    useEffect(() => {
        const interval = setInterval(() => { tick(); }, 1000);
        tick();
        return () => clearInterval(interval)
    }, [])

    return (
        <div className='popup-payment'>
            <div className="screen flex-center" onClick={handlerClickClosePopup}>
                <form method='POST' className="popup flex p-lg" onClick={(event) => event.stopPropagation()}>
                    <div className="close-btn pointer flex-center p-sm">
                        <i className="ai-cross"></i>
                    </div>
                    {/* <!-- CARD FORM --> */}
                    <div className="flex-fill flex-vertical">
                        <div className="header-pay flex-between flex-vertical-center">
                            <div className="flex-vertical-center">
                                <i className="ai-bitcoin-fill size-xl pr-sm f-main-color"></i>
                                <span className="title">
                                    <strong>FPT</strong><span>Pay</span>
                                </span>
                            </div>
                            <div className="timer" data-id="timer">
                                <span>0</span><span>5</span>
                                <em className='mx-5'>:</em>
                                <span>0</span><span>0</span>
                            </div>
                        </div>
                        <div className="card-data flex-fill flex-vertical">

                            {/* <!-- Card Number --> */}
                            <div className="flex-between flex-vertical-center">
                                <div className="card-property-title">
                                    <strong>Card Number</strong>
                                    <span>Enter 16-digit card number on the card</span>
                                </div>
                                {/* <div className="f-main-color pointer"><i className="fa-solid fa-pencil"></i> Edit</div> */}
                            </div>

                            {/* <!-- Card Field --> */}
                            <div className="flex-between">
                                <div className="card-number flex-vertical-center flex-fill">
                                    <div className="card-number-field flex-vertical-center flex-fill">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="24px" height="24px"><path fill="#ff9800" d="M32 10A14 14 0 1 0 32 38A14 14 0 1 0 32 10Z" /><path fill="#d50000" d="M16 10A14 14 0 1 0 16 38A14 14 0 1 0 16 10Z" /><path fill="#ff3d00" d="M18,24c0,4.755,2.376,8.95,6,11.48c3.624-2.53,6-6.725,6-11.48s-2.376-8.95-6-11.48 C20.376,15.05,18,19.245,18,24z" /></svg>

                                        <input onKeyUp={handleChangeNumberCard} name="num-1" className="numbers" type="text" maxLength={4} placeholder="0000" />-
                                        <input onKeyUp={handleChangeNumberCard} name="num-2" className="numbers" type="text" maxLength={4} placeholder="0000" />-
                                        <input onKeyUp={handleChangeNumberCard} name="num-3" className="numbers" type="text" maxLength={4} placeholder="0000" />-
                                        <input onKeyUp={handleChangeNumberCard} name="num-4" className="numbers" type="text" maxLength={4} placeholder="0000" />
                                    </div>
                                    <i className="ai-circle-check-fill size-lg f-main-color"></i>
                                </div>
                            </div>

                            {/* <!-- Expiry Date --> */}
                            <div className="flex-between">
                                <div className="card-property-title">
                                    <strong>Expiry Date</strong>
                                    <span>Enter the expiration date of the card</span>
                                </div>
                                <div className="card-property-value flex-vertical-center">
                                    <div className="input-container half-width">
                                        <input name='expiredMonth' onKeyUp={handlerChangeExpired} type="text" maxLength={2} placeholder="MM" />
                                    </div>
                                    <span className="m-md">/</span>
                                    <div className="input-container half-width">
                                        <input name='expiredYear' onKeyUp={handlerChangeExpired} type="text" maxLength={2} placeholder="YY" />
                                    </div>
                                </div>
                            </div>

                            {/* <!-- CCV Number --> */}
                            <div className="flex-between">
                                <div className="card-property-title">
                                    <strong>CVC Number</strong>
                                    <span>Enter card verification code from the back of the card</span>
                                </div>
                                <div className="card-property-value">
                                    <div className="input-container">
                                        <input onKeyUp={handleInputChange} name='cvc' id="cvc" type="password" maxLength={3} />
                                        <i id="cvc_toggler" data-target="cvc" className="fa-regular fa-eye pointer"></i>
                                    </div>
                                </div>
                            </div>

                            {/* <!-- Bank ID --> */}
                            <div className="flex-between">
                                <div className="card-property-title">
                                    <strong>Bank ID</strong>
                                    <span>Enter bank card ID</span>
                                </div>
                                <div className="card-property-value">
                                    {
                                        banks.map((bank: eBank) => {
                                            return <>
                                                <div key={bank.Id} className="input-container cusRadio">
                                                    <input onChange={handleInputChange} name='bankId' id="bankId" type="radio" value={bank.Id} />
                                                    <label className='d-flex align-items-center'>
                                                        <img src={bank.Logo} alt={bank.Name} />
                                                        <p className='fs-14'>{bank.Name}</p>
                                                    </label>
                                                </div>
                                            </>
                                        })
                                    }
                                </div>
                            </div>

                            {/* <!-- Name --> */}
                            <div className="flex-between">
                                <div className="card-property-title">
                                    <strong>Cardholder Name</strong>
                                    <span>Enter cardholder's name</span>
                                </div>
                                <div className="card-property-value">
                                    <div className="input-container">
                                        <input onKeyUp={handleInputChange} id="name" name='holderName' data-bound="name_mock" data-def="Mr. Cardholder" type="text" className="uppercase" placeholder="CARDHOLDER NAME" />
                                        <i className="fa-regular fa-user"></i>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div className="action flex-center">
                            <button onClick={handlerClickPayNow} type="submit" className="b-main-color pointer">Pay Now</button>
                        </div>
                    </div>

                    {/* <!-- SIDEBAR --> */}
                    <div className="sidebar flex-vertical">
                        <div>

                        </div>
                        <div className="purchase-section flex-fill flex-vertical">

                            <div className="card-mockup flex-vertical">
                                <div className="flex-fill justify-content-end">
                                    {/* <i className="ai-bitcoin-fill size-xl f-secondary-color"></i> */}
                                    <i className="fa-solid fa-wifi size-lg f-secondary-color"></i>
                                </div>
                                <div>
                                    <div id="name_mock" className="size-md pb-sm uppercase ellipsis">{values.holderName}</div>
                                    <div className="size-md pb-md">
                                        <strong>
                                            <span className="pr-sm">
                                                {values['num-1'] || "••••"}
                                            </span>
                                            <span className="pr-sm">
                                                &#x2022;&#x2022;&#x2022;&#x2022;
                                            </span>
                                            <span className="pr-sm">
                                                &#x2022;&#x2022;&#x2022;&#x2022;
                                            </span>
                                            <span className="pr-sm">
                                                {values['num-4'] || "••••"}
                                            </span>
                                        </strong>
                                    </div>
                                    <div className="flex-between flex-vertical-center">
                                        <strong className="size-md">
                                            <span id="mm_mock">{values.expiredMonth}</span>/<span id="yy_mock">{values.expiredYear}</span>
                                        </strong>

                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="24px" height="24px"><path fill="#ff9800" d="M32 10A14 14 0 1 0 32 38A14 14 0 1 0 32 10Z" /><path fill="#d50000" d="M16 10A14 14 0 1 0 16 38A14 14 0 1 0 16 10Z" /><path fill="#ff3d00" d="M18,24c0,4.755,2.376,8.95,6,11.48c3.624-2.53,6-6.725,6-11.48s-2.376-8.95-6-11.48 C20.376,15.05,18,19.245,18,24z" /></svg>
                                    </div>
                                </div>
                            </div>

                            <ul className="purchase-props">
                                <li className="flex-between">
                                    <strong>Film</strong>
                                    <span className='text-overflow'>{props.state.film.name}</span>
                                </li>
                                <li className="flex-between">
                                    <strong>Seats</strong>
                                    <CurrencyFormat value={props.state.totalPriceSeats} displayType={'text'} thousandSeparator={true} suffix={'đ'} />
                                </li>
                                <li className="flex-between">
                                    <strong>Combo</strong>
                                    <CurrencyFormat value={props.state.totalPriceCombo} displayType={'text'} thousandSeparator={true} suffix={'đ'} />
                                </li>
                            </ul>
                        </div>
                        <div className="separation-line"></div>
                        <div className="total-section flex-between flex-vertical-center">
                            <div className="flex-fill flex-vertical">
                                <div className="total-label f-secondary-color">You have to Pay</div>
                                <div>
                                    <strong><CurrencyFormat value={props.totalPrice} displayType={'text'} thousandSeparator={true} suffix={'đ'} /></strong>
                                </div>
                            </div>
                            <i className="fa-solid fa-hand-holding-dollar fs-20"></i>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    )
}
export default PopupPay;