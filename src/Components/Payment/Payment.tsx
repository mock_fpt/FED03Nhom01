import Format from 'Components/Common/Format';
import eConsession from 'Components/Interface/eConsession';
import eUserInfo from 'Components/Interface/eUserInfo';
import Footer from 'Components/UI/Footer/Footer'
import Header from 'Components/UI/Header/Header'
import Config from 'config';
import React, { useState, useEffect, ChangeEvent } from 'react'
import { useCookies } from 'react-cookie';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import APIs from 'service/APIs';
import './Payment.scss'
import PopupPay from './PopupPay/PopupPay';

var CurrencyFormat = require('react-currency-format');

const lstVoucher = [
    {
        code: 'VALENTINE',
        status: false,
        moneyPay: 0.8
    },
    {
        code: 'QUOCTEPHUNU',
        status: true,
        moneyPay: 0.75
    },
    {
        code: 'QUOCKHANH',
        status: true,
        moneyPay: 0.5
    },
]
interface eCard {
    "Name": string,
    "Logo": string,
    "CardNumber": string
}

function Payment() {
    const { state } = useLocation();
    const navigate = useNavigate();

    const [cookies, setCookies] = useCookies(['userInfo']);

    const [promotion, setPromotion] = useState("");
    const [displayPromotion, setDisplayPromotion] = useState({ code: '', discount: 0 });
    const [totalPrice, setTotalPrice] = useState(state.totalPriceCombo + state.totalPriceSeats);
    const [isOpenPopupPayment, setIsOpenPopupPayment] = useState(false);

    const [cards, setCards] = useState<Array<eCard>>([]);

    const [paymentMethod, setPaymentMethod] = useState("mastercard");

    useEffect(() => {
        fetch(APIs.GET_CARD_BY_EMAIL(cookies.userInfo.Email))
            .then(res => res.json())
            .then(data => {
                setCards(data);
            })
    }, []);

    const handlerChangePaymentMethod = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPaymentMethod(e.currentTarget.value);
    }


    const handlerApplyPromotion = () => {
        lstVoucher.forEach(item => {
            if (promotion === item.code && item.status === true) {
                setTotalPrice(item.moneyPay * (state.totalPriceCombo + state.totalPriceSeats));
                setDisplayPromotion({
                    code: item.code,
                    discount: 100 * (1 - item.moneyPay)
                })
            }
            else {
                setDisplayPromotion({
                    code: `${promotion} <span class='fs-12 fst-italic'>This code is invalid or has expired.</span>`,
                    discount: 0
                })
            }
        })
    }

    const handerClickPayment = () => {
        if (paymentMethod === 'mastercard') {
            setIsOpenPopupPayment(true);
        } else {
            // Pay card old
            const bodyCreateTicket = {
                "CardNumber": paymentMethod,
                "Price": totalPrice,
                "ShowCode": state.session.id,
                "Email": cookies.userInfo.Email,
                "CinemaName": state.cineplex.name,
                "TheaterName": state.session.screenName,
                "FilmName": state.film.name,
                "ImageLandscape": state.film.imageLandscape,
                "ImagePortrait": state.film.imagePortrait,
                "Combo": Format.getComboString(state.lstItemsChossed),
                "SeatCode": state.listSelectSeatsString.join(', '),
                "ShowTime": state.session.showDate.split("/").reverse().join("-") + 'T' + state.session.showTime + 'Z'
            }
            // Create Ticket
            fetch(APIs.CREATE_TICKET, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'accept': 'application/json'
                },
                body: JSON.stringify(bodyCreateTicket)
            })
                .then(res => {
                    if (res.status === 200) {
                        toast(`Chúc mừng bạn đã thanh toán đơn hàng thành công`, { type: 'success', autoClose: Config.durationToast });
                        setTimeout(() => {
                            navigate('/account/profile');
                        }, 2000);
                    } else {
                        toast(`Thẻ của bạn không đủ ${totalPrice}đ để thực hiện thanh toán!`, { type: 'warning', autoClose: Config.durationToast });
                    }
                })
        }
    }

    return (<>
        <Header />
        <div className='payment-page d-flex justify-content-center py-30'>
            <div className="mainSize d-flex">
                <div className="checkout w-50 border-box px-20">
                    <div className="title d-flex align-items-center mt-20 mb-15">
                        <span className={'d-inline-block fs-12 py-5 px-10 mr-10 ' + (state.film.age === "0" ? "age-p" : state.film.age === "13" ? "age-13" : state.film.age === "16" ? "age-16" : state.film.age === "18" ? "age-18" : "")}>{state.film.age !== "0" ? `${state.film.age}+` : "P"}</span>
                        <h3 className='d-inline-block fs-20'> {state.film.name}</h3>
                    </div>
                    <div className="divider-dash"></div>
                    <div className="ticket-details">
                        <div className="row my-15">
                            <div className="w-50 d-inline-block">
                                <h5>thời gian</h5>
                                <h3 className='fs-16'>{state.session.showTime} ~ {new Date(`01/01/1970 ${state.session.showTime}`).addHours(state.film.duration / 60).toTimeString().replace(/.*(\d{2}:\d{2}):\d{2}.*/, "$1")}</h3>
                            </div>
                            <div className="w-50 d-inline-block">
                                <h5>ngày chiếu</h5>
                                <h3 className='fs-16'>{state.session.showDate}</h3>
                            </div>
                        </div>
                        <div className="row my-15">
                            <h5>rạp</h5>
                            <h3 className='fs-16'>{state.cineplex.name}</h3>
                            <p className='fs-14'>{state.cineplex.address}</p>
                        </div>
                        <div className="row my-15">
                            <div className='w-50 d-inline-block'>
                                <h5>phòng chiếu</h5>
                                <h4>{state.session.screenName}</h4>
                            </div>
                            <div className='w-50 d-inline-block'>
                                <h5>định dạng</h5>
                                <h4>2D</h4>
                            </div>
                        </div>
                    </div>
                    <div className="divider-dash"></div>
                    <div className="seat-consession-details">
                        <div className="row my-15">
                            <h5>ghế</h5>
                            <div className="d-flex justify-content-between">
                                <h4>{state.listSelectSeatsString.join(', ')} </h4>
                                <h4><CurrencyFormat value={state.totalPriceSeats} displayType={'text'} thousandSeparator={true} suffix={'đ'} /></h4>
                            </div>
                        </div>
                        <div className="row my-15">
                            <h5>combo - bắp nước</h5>
                            {
                                state.lstItemsChossed.map((combo: eConsession, comboIndex: number) => {
                                    if (combo.defaultQuantity > 0) {
                                        return <div key={comboIndex} className="consession-item d-flex justify-content-between align-items-end">
                                            <h4 className='mr-15'>{combo.defaultQuantity} x {combo.description}:</h4>
                                            <h4><CurrencyFormat value={combo.displayPrice * combo.defaultQuantity} displayType={'text'} thousandSeparator={true} suffix={'đ'} /></h4>
                                        </div>
                                    }
                                })
                            }
                        </div>
                        <div className="row my-15">
                            <h5>Discount</h5>
                            <div className="d-flex justify-content-between">
                                <h4 className='seat-item' dangerouslySetInnerHTML={{ __html: displayPromotion.code }}></h4>
                                <h4>{displayPromotion.discount === 0 ? '' : `${displayPromotion.discount}%`}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="divider-dash"></div>
                    <div className="total-pay d-flex justify-content-between align-items-center my-15">
                        <h5>Tạm tính</h5>
                        <h4><CurrencyFormat value={totalPrice} displayType={'text'} thousandSeparator={true} suffix={'đ'} /></h4>
                    </div>
                </div>

                <div className="payment w-50 px-30 border-box">
                    <h2 className='text-center mt-20 mb-15 fs-20'>Thông tin tài khoản</h2>

                    <div className="divider-dash"></div>

                    <div className="row my-15 d-flex align-items-center justify-content-between">
                        <h5 className='w-20 mr-15'>Họ và tên</h5>
                        <input disabled value={cookies.userInfo.Name} type="text" className='flex-grow-1 border-box py-5 px-10' />
                    </div>
                    <div className="row my-15 d-flex align-items-center justify-content-between">
                        <h5 className='w-20 mr-15'>Email</h5>
                        <input disabled value={cookies.userInfo.Email} type="text" className='flex-grow-1 border-box py-5 px-10' />
                    </div>

                    <div className="divider-dash"></div>

                    <div className="row mt-15 mb-20">
                        <details>
                            <summary>
                                <h5 className='d-inline-block mb-10'>Mã khuyến mãi</h5>
                            </summary>
                            <div className="d-flex px-20">
                                <input onKeyUp={(e) => setPromotion(e.currentTarget.value)} type="text" placeholder='Enter a voucher code' className='flex-grow-1 border-box py-5 px-10 mr-10' />
                                <button onClick={handlerApplyPromotion} className='border-box px-15 py-5'>Áp dụng</button>
                            </div>
                        </details>
                    </div>

                    <div className="row my-15">
                        <h5 className='mb-10'>Hình thức thanh toán</h5>
                        <div className="radios px-30">

                            {/*  OLD Card */}
                            {
                                cards.map((card: eCard) => {
                                    return <div key={card.CardNumber} className="btn-radio d-flex align-items-center justify-content-between py-10 px-15 mb-10 border-box">
                                        <div className='d-flex align-items-center'>
                                            <img className='mr-15' src={card.Logo} alt="Logo" />
                                            <label htmlFor="mastercard">
                                                {card.CardNumber.substring(0, 4)} •••• •••• {card.CardNumber.substring(card.CardNumber.length - 4)}
                                            </label>
                                        </div>
                                        <input checked={paymentMethod === card.CardNumber} onChange={handlerChangePaymentMethod} className='border-box ml-15' value={card.CardNumber} name="payment-method" type="radio" />
                                    </div>
                                })
                            }

                            <div className="btn-radio d-flex align-items-center justify-content-between py-10 px-15 mb-10 border-box">
                                <div className='d-flex align-items-center'>
                                    <svg style={{ width: '24px' }} className="icon mr-15"><use xlinkHref="#icon-mastercard" /></svg>
                                    <label htmlFor="mastercard">
                                        MasterCard
                                    </label>
                                </div>
                                <input checked={paymentMethod === 'mastercard'} onChange={handlerChangePaymentMethod} className='border-box ml-15' value="mastercard" name="payment-method" type="radio" />
                            </div>
                        </div>
                    </div>

                    <div className="row my-15">
                        <button className='btn-payment w-100 border-box py-10' title='Xác nhận thanh toán' onClick={handerClickPayment}>
                            <span className='d-inline-block position-relative'>Thanh toán</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {
            isOpenPopupPayment && <PopupPay totalPrice={totalPrice} state={state} setOpenPopup={setIsOpenPopupPayment} />
        }
        <Footer />
        <svg xmlns="http://www.w3.org/2000/svg" style={{ display: "none", }}>
            <symbol id="icon-mastercard" viewBox="0 0 504 504">
                <path d="m504 252c0 83.2-67.2 151.2-151.2 151.2-83.2 0-151.2-68-151.2-151.2 0-83.2 67.2-151.2 150.4-151.2 84.8 0 152 68 152 151.2z" fill="#ffb600" />
                <path d="m352.8 100.8c83.2 0 151.2 68 151.2 151.2 0 83.2-67.2 151.2-151.2 151.2-83.2 0-151.2-68-151.2-151.2" fill="#f7981d" />
                <path d="m352.8 100.8c83.2 0 151.2 68 151.2 151.2 0 83.2-67.2 151.2-151.2 151.2" fill="#ff8500" />
                <path d="m149.6 100.8c-82.4.8-149.6 68-149.6 151.2s67.2 151.2 151.2 151.2c39.2 0 74.4-15.2 101.6-39.2 5.6-4.8 10.4-10.4 15.2-16h-31.2c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8 6.4-10.4 8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2c4.8-15.2 8-31.2 8-48 0-11.2-1.6-21.6-3.2-32h-92.8c.8-5.6 2.4-10.4 4-16h83.2c-1.6-5.6-4-11.2-6.4-16h-70.4c2.4-5.6 5.6-10.4 8.8-16h53.6c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6 9.6-10.4 15.2-15.2-26.4-24.8-62.4-39.2-101.6-39.2 0-1.6 0-1.6-.8-1.6z" fill="#ff5050" />
                <path d="m0 252c0 83.2 67.2 151.2 151.2 151.2 39.2 0 74.4-15.2 101.6-39.2 5.6-4.8 10.4-10.4 15.2-16h-31.2c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8 6.4-10.4 8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2c4.8-15.2 8-31.2 8-48 0-11.2-1.6-21.6-3.2-32h-92.8c.8-5.6 2.4-10.4 4-16h83.2c-1.6-5.6-4-11.2-6.4-16h-70.4c2.4-5.6 5.6-10.4 8.8-16h53.6c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6 9.6-10.4 15.2-15.2-26.4-24.8-62.4-39.2-101.6-39.2h-.8" fill="#e52836" />
                <path d="m151.2 403.2c39.2 0 74.4-15.2 101.6-39.2 5.6-4.8 10.4-10.4 15.2-16h-31.2c-4-4.8-8-10.4-11.2-15.2h53.6c3.2-4.8 6.4-10.4 8.8-16h-71.2c-2.4-4.8-4.8-10.4-6.4-16h83.2c4.8-15.2 8-31.2 8-48 0-11.2-1.6-21.6-3.2-32h-92.8c.8-5.6 2.4-10.4 4-16h83.2c-1.6-5.6-4-11.2-6.4-16h-70.4c2.4-5.6 5.6-10.4 8.8-16h53.6c-3.2-5.6-7.2-11.2-12-16h-29.6c4.8-5.6 9.6-10.4 15.2-15.2-26.4-24.8-62.4-39.2-101.6-39.2h-.8" fill="#cb2026" />
                <g fill="#fff">
                    <path d="m204.8 290.4 2.4-13.6c-.8 0-2.4.8-4 .8-5.6 0-6.4-3.2-5.6-4.8l4.8-28h8.8l2.4-15.2h-8l1.6-9.6h-16s-9.6 52.8-9.6 59.2c0 9.6 5.6 13.6 12.8 13.6 4.8 0 8.8-1.6 10.4-2.4z" />
                    <path d="m210.4 264.8c0 22.4 15.2 28 28 28 12 0 16.8-2.4 16.8-2.4l3.2-15.2s-8.8 4-16.8 4c-17.6 0-14.4-12.8-14.4-12.8h32.8s2.4-10.4 2.4-14.4c0-10.4-5.6-23.2-23.2-23.2-16.8-1.6-28.8 16-28.8 36zm28-23.2c8.8 0 7.2 10.4 7.2 11.2h-17.6c0-.8 1.6-11.2 10.4-11.2z" />
                    <path d="m340 290.4 3.2-17.6s-8 4-13.6 4c-11.2 0-16-8.8-16-18.4 0-19.2 9.6-29.6 20.8-29.6 8 0 14.4 4.8 14.4 4.8l2.4-16.8s-9.6-4-18.4-4c-18.4 0-36.8 16-36.8 46.4 0 20 9.6 33.6 28.8 33.6 6.4 0 15.2-2.4 15.2-2.4z" />
                    <path d="m116.8 227.2c-11.2 0-19.2 3.2-19.2 3.2l-2.4 13.6s7.2-3.2 17.6-3.2c5.6 0 10.4.8 10.4 5.6 0 3.2-.8 4-.8 4s-4.8 0-7.2 0c-13.6 0-28.8 5.6-28.8 24 0 14.4 9.6 17.6 15.2 17.6 11.2 0 16-7.2 16.8-7.2l-.8 6.4h14.4l6.4-44c0-19.2-16-20-21.6-20zm3.2 36c0 2.4-1.6 15.2-11.2 15.2-4.8 0-6.4-4-6.4-6.4 0-4 2.4-9.6 14.4-9.6 2.4.8 3.2.8 3.2.8z" />
                    <path d="m153.6 292c4 0 24 .8 24-20.8 0-20-19.2-16-19.2-24 0-4 3.2-5.6 8.8-5.6 2.4 0 11.2.8 11.2.8l2.4-14.4s-5.6-1.6-15.2-1.6c-12 0-24 4.8-24 20.8 0 18.4 20 16.8 20 24 0 4.8-5.6 5.6-9.6 5.6-7.2 0-14.4-2.4-14.4-2.4l-2.4 14.4c.8 1.6 4.8 3.2 18.4 3.2z" />
                    <path d="m472.8 214.4-3.2 21.6s-6.4-8-15.2-8c-14.4 0-27.2 17.6-27.2 38.4 0 12.8 6.4 26.4 20 26.4 9.6 0 15.2-6.4 15.2-6.4l-.8 5.6h16l12-76.8zm-7.2 42.4c0 8.8-4 20-12.8 20-5.6 0-8.8-4.8-8.8-12.8 0-12.8 5.6-20.8 12.8-20.8 5.6 0 8.8 4 8.8 13.6z" />
                    <path d="m29.6 291.2 9.6-57.6 1.6 57.6h11.2l20.8-57.6-8.8 57.6h16.8l12.8-76.8h-26.4l-16 47.2-.8-47.2h-23.2l-12.8 76.8z" />
                    <path d="m277.6 291.2c4.8-26.4 5.6-48 16.8-44 1.6-10.4 4-14.4 5.6-18.4 0 0-.8 0-3.2 0-7.2 0-12.8 9.6-12.8 9.6l1.6-8.8h-15.2l-10.4 62.4h17.6z" />
                    <path d="m376.8 227.2c-11.2 0-19.2 3.2-19.2 3.2l-2.4 13.6s7.2-3.2 17.6-3.2c5.6 0 10.4.8 10.4 5.6 0 3.2-.8 4-.8 4s-4.8 0-7.2 0c-13.6 0-28.8 5.6-28.8 24 0 14.4 9.6 17.6 15.2 17.6 11.2 0 16-7.2 16.8-7.2l-.8 6.4h14.4l6.4-44c.8-19.2-16-20-21.6-20zm4 36c0 2.4-1.6 15.2-11.2 15.2-4.8 0-6.4-4-6.4-6.4 0-4 2.4-9.6 14.4-9.6 2.4.8 2.4.8 3.2.8z" />
                    <path d="m412 291.2c4.8-26.4 5.6-48 16.8-44 1.6-10.4 4-14.4 5.6-18.4 0 0-.8 0-3.2 0-7.2 0-12.8 9.6-12.8 9.6l1.6-8.8h-15.2l-10.4 62.4h17.6z" />
                </g>
                <path d="m180 279.2c0 9.6 5.6 13.6 12.8 13.6 5.6 0 10.4-1.6 12-2.4l2.4-13.6c-.8 0-2.4.8-4 .8-5.6 0-6.4-3.2-5.6-4.8l4.8-28h8.8l2.4-15.2h-8l1.6-9.6" fill="#dce5e5" />
                <path d="m218.4 264.8c0 22.4 7.2 28 20 28 12 0 16.8-2.4 16.8-2.4l3.2-15.2s-8.8 4-16.8 4c-17.6 0-14.4-12.8-14.4-12.8h32.8s2.4-10.4 2.4-14.4c0-10.4-5.6-23.2-23.2-23.2-16.8-1.6-20.8 16-20.8 36zm20-23.2c8.8 0 10.4 10.4 10.4 11.2h-20.8c0-.8 1.6-11.2 10.4-11.2z" fill="#dce5e5" />
                <path d="m340 290.4 3.2-17.6s-8 4-13.6 4c-11.2 0-16-8.8-16-18.4 0-19.2 9.6-29.6 20.8-29.6 8 0 14.4 4.8 14.4 4.8l2.4-16.8s-9.6-4-18.4-4c-18.4 0-28.8 16-28.8 46.4 0 20 1.6 33.6 20.8 33.6 6.4 0 15.2-2.4 15.2-2.4z" fill="#dce5e5" />
                <path d="m95.2 244.8s7.2-3.2 17.6-3.2c5.6 0 10.4.8 10.4 5.6 0 3.2-.8 4-.8 4s-4.8 0-7.2 0c-13.6 0-28.8 5.6-28.8 24 0 14.4 9.6 17.6 15.2 17.6 11.2 0 16-7.2 16.8-7.2l-.8 6.4h14.4l6.4-44c0-18.4-16-19.2-22.4-19.2m12 34.4c0 2.4-9.6 15.2-19.2 15.2-4.8 0-6.4-4-6.4-6.4 0-4 2.4-9.6 14.4-9.6 2.4.8 11.2.8 11.2.8z" fill="#dce5e5" />
                <path d="m136 290.4s4.8 1.6 18.4 1.6c4 0 24 .8 24-20.8 0-20-19.2-16-19.2-24 0-4 3.2-5.6 8.8-5.6 2.4 0 11.2.8 11.2.8l2.4-14.4s-5.6-1.6-15.2-1.6c-12 0-16 4.8-16 20.8 0 18.4 12 16.8 12 24 0 4.8-5.6 5.6-9.6 5.6" fill="#dce5e5" />
                <path d="m469.6 236s-6.4-8-15.2-8c-14.4 0-19.2 17.6-19.2 38.4 0 12.8-1.6 26.4 12 26.4 9.6 0 15.2-6.4 15.2-6.4l-.8 5.6h16l12-76.8m-20.8 41.6c0 8.8-7.2 20-16 20-5.6 0-8.8-4.8-8.8-12.8 0-12.8 5.6-20.8 12.8-20.8 5.6 0 12 4 12 13.6z" fill="#dce5e5" />
                <path d="m29.6 291.2 9.6-57.6 1.6 57.6h11.2l20.8-57.6-8.8 57.6h16.8l12.8-76.8h-20l-22.4 47.2-.8-47.2h-8.8l-27.2 76.8z" fill="#dce5e5" />
                <path d="m260.8 291.2h16.8c4.8-26.4 5.6-48 16.8-44 1.6-10.4 4-14.4 5.6-18.4 0 0-.8 0-3.2 0-7.2 0-12.8 9.6-12.8 9.6l1.6-8.8" fill="#dce5e5" />
                <path d="m355.2 244.8s7.2-3.2 17.6-3.2c5.6 0 10.4.8 10.4 5.6 0 3.2-.8 4-.8 4s-4.8 0-7.2 0c-13.6 0-28.8 5.6-28.8 24 0 14.4 9.6 17.6 15.2 17.6 11.2 0 16-7.2 16.8-7.2l-.8 6.4h14.4l6.4-44c0-18.4-16-19.2-22.4-19.2m12 34.4c0 2.4-9.6 15.2-19.2 15.2-4.8 0-6.4-4-6.4-6.4 0-4 2.4-9.6 14.4-9.6 3.2.8 11.2.8 11.2.8z" fill="#dce5e5" />
                <path d="m395.2 291.2h16.8c4.8-26.4 5.6-48 16.8-44 1.6-10.4 4-14.4 5.6-18.4 0 0-.8 0-3.2 0-7.2 0-12.8 9.6-12.8 9.6l1.6-8.8" fill="#dce5e5" />
            </symbol>
        </svg>

    </>
    )
}

export default Payment