import React, { useState, useEffect } from 'react'

import { Link, NavLink, useNavigate } from 'react-router-dom';
import APIs from 'service/APIs';
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import '../Login/Login.scss';
import useCookies from 'react-cookie/cjs/useCookies';
import Config from 'config';


export default function Register() {

    const navigate = useNavigate();
    const [cookies, setCookies, removeCookies] = useCookies(['userInfo']);

    const [email, setEmail] = useState("");
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");

    const handlerCheckRegister = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // Validate
        if(!name || !password || !email) {
            toast(`Hãy nhập dữ liệu đầy đủ!`, {type: 'error', autoClose: Config.durationToast})
            return;
        }

        const requestOptions = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify({
                "Email": email,
                "Name": name,
                "Password": password,
                "Role": "0"
            })
        }

        fetch(APIs.POST_REGISTER, requestOptions)
            .then((res) => {
                if (res.status === 200) {
                    res.text().then(result => {
                        if (result === "Create Fail") {
                            toast("Đăng ký thất bại", { type: 'error', autoClose: Config.durationToast })
                        } else {
                            removeCookies('userInfo', { path: '/' });
                            setCookies("userInfo", JSON.stringify({
                                "Email": email,
                                "Name": name,
                                "Role": "0"
                            }));
                            toast(`Đăng ký thành công, xin chào ${name}`, { type: 'success', autoClose: Config.durationToast });
                            setTimeout(() => {
                                navigate("/");
                            }, 2000)
                        }
                    })
                } else {
                    toast("Có lỗi hệ thống!", { type: 'warning' })
                }
            })
    }

    return (
        <form onSubmit={handlerCheckRegister} className='login h-100vh d-flex justify-content-center align-items-center'>
            <div className='main-form position-relative border-box'>
                <h1 className='mb-20'>Đăng ký</h1>
                <div className='form-group mb-20'>
                    <input className='w-100 border-box fs-16 py-15 border-0 border-rad-8 px-15' onKeyUp={(e) => setName(e.currentTarget.value)} type="text" placeholder='Họ và tên' />
                </div>
                <div className='form-group mb-20'>
                    <input className='w-100 border-box fs-16 py-15 border-0 border-rad-8 px-15' onKeyUp={(e) => setEmail(e.currentTarget.value)} type="text" placeholder='Email hoặc số điện thoại' />
                </div>
                <div className='form-group mb-20'>
                    <input className='w-100 border-box fs-16 py-15 border-0 border-rad-8 px-15' onKeyUp={(e) => setPassword(e.currentTarget.value)} type="password" placeholder='Mật khẩu' />
                </div>

                <button className='text-uppercase border-0 button w-100 fw-bolder fs-16 py-15 mb-15'>Đăng ký</button>
                <p className='fs-14 my-5'>Bạn đã tài khoản? <Link to={'/account/login'} className='ml-10 text-decoration-none fs-16 fw-bolder'>Đăng nhập ngay</Link></p>

                <div className='x-cancel d-flex justify-content-center align-items-center position-absolute'>
                    <span onClick={() => navigate("/")}><i className="fa-regular fa-circle-xmark fs-45"></i></span>
                </div>
            </div>
        </form>
    )
}
