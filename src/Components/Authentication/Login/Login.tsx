import React, { useState, useEffect, useRef } from 'react'
import { useNavigate, Link, useLocation } from 'react-router-dom';
import APIs from 'service/APIs';
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import './Login.scss';
import eUserInfo from 'Components/Interface/eUserInfo';
import useCookies from 'react-cookie/cjs/useCookies';
import Config from 'config';


export default function Login() {

    const navigate = useNavigate();
    const myRef = useRef<any>(null);

    const [cookies, setCookies, removeCookies] = useCookies(['userInfo']);

    const {state} = useLocation();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");


    useEffect(() => {
        if(state?.toast) {
            toast(state.toast, {
                type: 'warning',
                position: 'top-center',
                autoClose: Config.durationToast
            });
        }
        myRef.current?.scrollTo(0, 0);
    }, [])

    const handlerCheckLogin = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // Validate
        if(!username || !password) {
            toast(`Hãy nhập tài khoản và mật khẩu!`, {type: 'error', autoClose: Config.durationToast})
            return;
        }

        const requestOptions = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify({
                "Email": username,
                "Password": password
            })
        }

        fetch(APIs.POST_LOGIN, requestOptions)
        .then(res => {
            if (res.status === 404) {
                removeCookies('userInfo', { path: '/' });
                toast("Sai tên người dùng hoặc mật khẩu", {
                    type: 'error',
                    autoClose: Config.durationToast
                });
            } else {
                res.json().then((result: eUserInfo) => {
                    setCookies("userInfo", JSON.stringify(result));
                    toast("Chào mừng " + result["Name"], {
                        type: 'success',
                        autoClose: Config.durationToast
                    })
                    setTimeout(() => {

                        if(state?.isGotoPayment) {
                            navigate("/booking/payment", {
                                state: state
                            });
                            return;
                        }

                        if(state?.prevPage) {
                            navigate(state.prevPage, {
                                state: state
                            });
                        } else {
                            navigate("/");
                        }
                    }, 2000)
                })
            }
        })
    }


    return (
        <form ref={myRef} onSubmit={handlerCheckLogin} className='login h-100vh d-flex justify-content-center align-items-center'>
            <div className='main-form position-relative border-box'>
                <h1 className='mb-20'>Đăng nhập</h1>

                <div className='form-group mb-20'>
                    <input className='w-100 border-box fs-16 py-15 border-0 border-rad-8 px-15' onKeyUp={(e) => setUsername(e.currentTarget.value)} onChange={(e) => setUsername(e.currentTarget.value)} type="email" placeholder='Email hoặc số điện thoại' />
                </div>

                <div className='form-group mb-20'>
                    <input className='w-100 border-box fs-16 py-15 border-0 border-rad-8 px-15' onKeyUp={(e) => setPassword(e.currentTarget.value)} onChange={(e) => setPassword(e.currentTarget.value)} type="password" placeholder='Mật khẩu' />
                </div>

                <button className='text-uppercase border-0 button w-100 fw-bolder fs-16 py-15 mb-15' type='submit'>Đăng nhập</button>
                <p className='fs-14 my-5'>Bạn chưa có tài khoản? <Link to={'/account/register'} className='ml-10 text-decoration-none fs-16 fw-bolder'>Đăng ký ngay</Link></p>
                <p className='fs-12'>Trang này được Google reCAPTCHA bảo vệ để đảm bảo bạn không phải là robot. Tìm hiểu thêm.</p>

                <div className='x-cancel d-flex justify-content-center align-items-center position-absolute'>
                    <span onClick={() => navigate("/")}><i className="fa-regular fa-circle-xmark fs-45"></i></span>
                </div>
            </div>
        </form>
    )
}
