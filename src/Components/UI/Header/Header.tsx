import React, { useState, useRef, useEffect } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import useCookies from 'react-cookie/cjs/useCookies';

import "./Header.scss";

const menuItems = [
    {
        Title: 'Lịch chiếu phim',
        Url: '/lich-chieu'
    },
    {
        Title: 'Rạp phim',
        Url: '/cinema'
    },
    {
        Title: 'Blog điện ảnh',
        Url: '/blog-dien-anh'
    },
]

export default function Header() {
    const navigate = useNavigate();

    const [cookies, setCookies, removeCookies] = useCookies(['userInfo']);
    const ref = useRef<any>(null);
    const [heightHeader, setHeightHeader] = useState(0);

    const handleLogout = () => {
        removeCookies('userInfo', { path: '/' });
        navigate('/');
    }
    useEffect(() => {
        setHeightHeader(ref.current.offsetHeight);
    }, [])

    return (
        <div id='header' className="header" style={{ marginBottom: `${heightHeader}px` }}>
            <div ref={ref} className={`d-flex justify-content-center position-fixed w-100`}>
                <div className="mainSize d-flex align-items-center justify-content-between">
                    <Link to={'/'} className='brand d-inline-block text-decoration-none'>
                        <div className="d-flex align-items-center">
                            <img className="mr-5" src={require("./img/logo2.png")} alt="brand" />
                            <span className="fs-18 fw-bold">TuKaCine</span>
                        </div>
                    </Link>
                    <div className="nav">
                        {
                            menuItems.map((e) => {
                                return <>
                                    <NavLink key={e.Title} className="d-inline-block p-10 mx-5 text-decoration-none" to={`${e.Url}`}>{e.Title}</NavLink>
                                </>
                            })
                        }
                        {
                            cookies.userInfo
                                ? <>
                                    <NavLink key='login' className="d-inline-block p-10 mx-5 text-decoration-none" to={'/account/profile'}>
                                        <i className="fa-regular fa-circle-user mr-5"></i>{[...cookies.userInfo.Name.split(' ')].pop()}
                                    </NavLink>
                                    <button className='fs-16 d-inline-block p-10 mx-5 border-0' onClick={() => handleLogout()}><i className="fa fa-sign-out" aria-hidden="true"></i> Log out</button>
                                </>
                                : <>
                                    <NavLink key='login' className="d-inline-block p-10 mx-5 text-decoration-none" to={'/account/login'}>
                                        Login<i className="ml-5 fa-solid fa-arrow-right-to-bracket"></i>
                                    </NavLink>

                                </>
                        }
                        {/* <NavLink key='login' className="d-inline-block p-10 mx-5 text-decoration-none" to={cookies.userInfo ? `/account/profile` : '/account/login'}>
                            {
                                cookies.userInfo
                                    ? <><i className="fa-regular fa-circle-user mr-5"></i>{[...cookies.userInfo.Name.split(' ')].pop()}</>
                                    : <>Login<i className="ml-5 fa-solid fa-arrow-right-to-bracket"></i></>
                            }
                        </NavLink> */}
                    </div>
                </div>
            </div>
        </div>
    );
}
