import React, { useState } from 'react'
import { eMovieInfo } from 'Components/Interface/eMovieInfo';
import { useNavigate } from 'react-router-dom';
import ModalTrailer from '../ModalTrailer/ModalTrailer';

import './CardMovie.scss'

interface props {
    infoMovie: eMovieInfo,
    sizeItem?: string
}

function CardMovie(props: props) {
    const navigate = useNavigate();
    const [isOpenModal, setIsOpen] = useState(false)

    return (<>
        <div className={`item-movie p-10 border-box ${(props.sizeItem) ? props.sizeItem : 'w-25'}`}>
            <div className='position-relative' onClick={() => setIsOpen(true)}>
                <img src={`${props.infoMovie.imagePortrait}`}
                    alt="poster movie" className='w-100' />
                <i className="fa-regular fa-circle-play position-absolute"></i>
            </div>
            <div className="content">
                <p className='fs-18 fw-bold'>{props.infoMovie.name}</p>
                <p>{props.infoMovie.subName ? props.infoMovie.subName : ""}</p>
            </div>
            <button className='btn-movie w-100 py-10 my-5 text-decoration-none d-inline-block text-center fs-16'
                onClick={() => navigate(`/movie/${props.infoMovie.id}`)}>
                Xem chi tiết
            </button>
        </div>
        {
            isOpenModal && <ModalTrailer setOpenFunc={setIsOpen} infoMovie={props.infoMovie} />
        }
    </>
    )
}

export default CardMovie;