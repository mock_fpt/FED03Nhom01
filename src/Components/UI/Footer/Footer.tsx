import React from 'react'
import { Link } from 'react-router-dom'
import "./Footer.scss"

function Footer() {
    return (
        <div className='footer w-100'>
            <div className='d-flex justify-content-center'>
                <div className="mainSize border-box py-20">
                    <div className="footer-top d-flex py-10">
                        <div className='flex-grow-2'>
                            <h6 className='text-uppercase pl-5 mb-15'>giới thiệu</h6>
                            <p className='pl-10'>Về chúng tôi</p>
                            <p className='pl-10'>Điều khoản sử dụng</p>
                            <p className='pl-10'>Quy chế hoạt động</p>
                            <p className='pl-10'>Chính sách bảo mật</p>
                        </div>
                        <div className='flex-grow-2'>
                            <h6 className='text-uppercase pl-5 mb-15'>lịch chiếu phim</h6>
                            <Link className='d-block pl-10 text-decoration-none' to={'/cinema'}>Rạp phim</Link>
                            <Link className='d-block pl-10 text-decoration-none' to={'/lich-chieu#now-showing'}>Phim đang chiếu</Link>
                            <Link className='d-block pl-10 text-decoration-none' to={'/lich-chieu#coming-soon'}>Phim sắp chiếu</Link>
                        </div>
                        <div className='flex-grow-3'>
                            <h6 className='text-uppercase pl-5 mb-15'>hỗ trợ khách hàng</h6>
                            <p className='pl-10'>Địa chỉ: Số 1 Võ Văn Ngân, Thành phố Thủ Đức.</p>
                            <p className='pl-10'>Hotline: 1900 6610</p>
                            <p className='pl-10'>Email: tuka@gmail.com</p>
                            <div className='d-flex mt-5'>
                                <div className='app d-flex align-items-center mx-15'>
                                    <img src={require('./img/app-store.png')} alt="logo app" className='iconSize mr-5' />
                                    <div>
                                        <p className='fs-12'>Tải về trên</p>
                                        <p className='fs-14 fw-bold'>AppStore</p>
                                    </div>
                                </div>
                                <div className='app d-flex align-items-center mx-15'>
                                    <img src={require('./img/google-play.png')} alt="logo app" className='iconSize mr-5' />
                                    <div>
                                        <p className='fs-12'>Tải nội dung trên</p>
                                        <p className='fs-14 fw-bold'>Google Play</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="footer-bottom d-flex align-items-center pt-10">
                        <div className='d-flex align-items-center flex-grow-2'>
                            <img src={require('./img/logo2.png')} alt="logo" className='iconSize mr-20' />
                            <div>
                                <p className='fw-bold fs-20'>TukaCine</p>
                                <p>Số 1 Võ Văn Ngân, Thành phố Thủ Đức, Việt Nam</p>
                            </div>
                        </div>
                        <div className='flex-grow-1'>
                            <img src={require('./img/icon-facebook.png')} alt="icon social" className='iconSize mx-5' />
                            <img src={require('./img/icon-linkedin.png')} alt="icon social" className='iconSize mx-5' />
                            <img src={require('./img/icon-youtube.png')} alt="icon social" className='iconSize mx-5' />
                            <p className='fs-12 fw-bold'>@Copyright 2023 - Trung & Khang</p>
                        </div>
                        <div className='certificate flex-grow-1'>
                            <p className='fs-14'>Chứng nhận bởi</p>
                            <img src={require('./img/certificate.png')} alt="icon certificate" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Footer