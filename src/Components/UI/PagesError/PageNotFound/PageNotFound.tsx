import React from 'react'
import { Link } from 'react-router-dom';
import './PageNotFound.scss'

const lstContentPageNotPound = [
    {
        id: 1,
        title: '404 PAGE NOT FOUND',
        sub: "HYDRA has stolen this page from the S.H.I.E.L.D. database!",
        urlImg: './error1.jpg',
        urlImgBg: '',
    },
    {
        id: 2,
        title: '404 PAGE NOT FOUND',
        sub: "Protocol missing... Exiting program...",
        urlImg: './error2.jpg',
        urlImgBg: './error2-bg.gif',
    },
    {
        id: 3,
        title: '404 PAGE NOT FOUND',
        sub: "$#&%, you broke something! Just kidding...",
        urlImg: './error3.gif',
        urlImgBg: '',
    },
    {
        id: 4,
        title: '404 PAGE NOT FOUND',
        sub: "Not even the Eye of Uatu sees your request...",
        urlImg: './error4.jpg',
        urlImgBg: '',
    },
    {
        id: 5,
        title: '404 PAGE NOT FOUND',
        sub: "HYDRA is currently attacking this page!",
        urlImg: './error5.jpg',
        urlImgBg: '',
    },
    {
        id: 6,
        title: '404 PAGE NOT FOUND',
        sub: "You are not worthy...",
        urlImg: './error6.png',
        urlImgBg: './error6-bg.jpg',
    },
]


function PageNotFound() {
    return (
        <div className='page-not-found d-flex h-100vh'>
            <div className="content-not-pound text-center border-box">
                <h1 className='fs-64 fw-bolder test'>404 PAGE NOT FOUND</h1>
                <p className='fs-30'>"HYDRA is currently attacking this page!"</p>
                <Link to={'/'} className='border-box my-15 d-inline-block text-decoration-none fs-20 px-30 py-10'>Go back Home</Link>
            </div>
            <div className="img-not-pound  border-box">
            </div>
        </div>
    )
}

export default PageNotFound;