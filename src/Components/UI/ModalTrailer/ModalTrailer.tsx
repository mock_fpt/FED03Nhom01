import React from 'react'
import { useNavigate } from 'react-router-dom';
import './ModalTrailer.scss'

import { eMovieInfo } from 'Components/Interface/eMovieInfo';

interface ModalProps {
    infoMovie: eMovieInfo;
    setOpenFunc: (isOpen: boolean) => void,
}

function ModalTrailer(props: ModalProps) {
    const navigate = useNavigate();
    const handleCancel = (event: any) => {
        stopEventPropagationTry(event);
        props.setOpenFunc(false);
    }

    const stopEventPropagationTry = (event: any) => {
        if (event.target === event.currentTarget) event.stopPropagation();
    }
    const convertLinkTrailer = (link: String) => {
        return `https://www.youtube.com/embed/${link.replace("https://www.youtube.com/watch?v=", "")}?start=1`
    }

    return (
        <div className={`modal d-flex align-items-center justify-content-center d-inline-block`} onClick={() => props.setOpenFunc(false)}>
            <div className='p-20 border-box border-rad-8 position-relative' onClick={(event) => event.stopPropagation()}>
                <div className="trailer border-box">
                    <iframe width="100%" height="365px" title="YouTube video player" frameBorder="0" allowFullScreen
                        src={convertLinkTrailer(props.infoMovie.trailer)}
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share">
                    </iframe>
                </div>
                <div className="content d-flex">
                    <div className="poster mr-10 ">
                        <img src={`${props.infoMovie.imagePortrait}`} alt="poster movie" />
                    </div>
                    <div className="desc border-box d-flex flex-col">
                        <h5 className='fs-25 my-5'>{props.infoMovie.name}</h5>
                        <p className='fs-14 mb-10'>
                            {props.infoMovie.shortDescription
                                ? convertToPlain(props.infoMovie.shortDescription)
                                : convertToPlain(props.infoMovie.description)
                            }
                        </p>

                        <div className="btn">
                            <button className='d-inline-block text-decoration-none py-5 px-20 border-box mr-15 fs-14'
                                onClick={() => navigate(`/movie/${props.infoMovie.id}`)}>
                                Đặt vé
                            </button>
                            <button className='py-5 px-20 border-box fs-14'
                                onClick={(event) => handleCancel(event)}>Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function convertToPlain(html: any) {
    // Create a new div element
    let tempDivElement = document.createElement('div');
    // Set the HTML content with the given value
    tempDivElement.innerHTML = html;
    // Retrieve the text property of the element
    return tempDivElement.textContent || tempDivElement.innerText || "";
}

export default ModalTrailer;