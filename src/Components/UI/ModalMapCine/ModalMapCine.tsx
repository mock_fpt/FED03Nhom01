import eCinema from 'Components/Interface/eCinema'
import React from 'react'
import './ModalMapCine.scss'

interface eModalMap {
    Cine: eCinema | undefined
    setOpenFunc: (isOpen: boolean) => void,
}

function ModalMapCine(props: eModalMap) {
    return (
        <div className={`modalMap d-flex align-items-center justify-content-center d-inline-block`} onClick={() => props.setOpenFunc(false)}>
            <div className='py-20 border-box border-rad-15 position-relative' onClick={(event) => event.stopPropagation()}>
                <div className="title-modalMap mx-20 mb-10">
                    <h3 className='text-center position-relative fs-20'>Bản đồ
                        <span className='position-absolute px-10' onClick={() => props.setOpenFunc(false)}>
                            <i className="fa-solid fa-xmark fs-18"></i>
                        </span>
                    </h3>
                </div>
                <div className="content-modalmap">
                    {
                        props.Cine && <div className="trailer border-box">
                            <iframe src={`${props.Cine.mapEmbeb}}`} width="100%" height="400px" allowFullScreen loading="lazy" title='Map Cinema Embed' referrerPolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                    }
                </div>
                <div className='mt-10 d-flex justify-content-end mr-20'>
                    <button className="px-30 py-10 border-rad-8 border-0 border-box fs-16" onClick={() => props.setOpenFunc(false)}>Đóng</button>
                </div>
            </div>
        </div>
    )
}

export default ModalMapCine