import React from 'react'
import { Link, useLocation, Routes } from 'react-router-dom';
import './Breadcrumbs.scss'

function Breadcrumbs() {
    const location = useLocation();
    console.log(location, Routes)
    return (
        <nav>
            <Link to="/"
                className={location.pathname === "/" ? "breadcrumb-active" : "breadcrumb-not-active"}
            >
                Home
            </Link>
            <Link to="/products"
                className={location.pathname.startsWith("/products") ? "breadcrumb-active" : "breadcrumb-not-active"}
            >
                Products
            </Link>
            <Link to="/products/1"
                className={location.pathname === "/products/1" ? "breadcrumb-active" : "breadcrumb-not-active"}
            >
                Product 1
            </Link>
        </nav>
    )
}
export default Breadcrumbs;