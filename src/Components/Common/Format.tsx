import eConsession from "Components/Interface/eConsession";

const getComboString = (lstItemsChossed : Array<eConsession>) : string => {
    let result = lstItemsChossed.filter((item : eConsession) => {
        return item.defaultQuantity > 0;
    }).map((item : eConsession) => {
        return (item.description + " X " + item.defaultQuantity);
    })
    return result.join(', ');   
}

const Format = {
    getComboString: getComboString
}

export default Format;