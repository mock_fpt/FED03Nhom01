import eUserInfo from "Components/Interface/eUserInfo";

interface clientStorage {
    userInfo: eUserInfo,
    isLogged: () => boolean
    clear: () => void,
}

function getCookie(cname: string) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const clientStorage : clientStorage = {
    userInfo: JSON.parse(localStorage.getItem("userInfo") || "{\"Email\":\"\",\"Name\":\"\",\"Role\":\"\"}"),
    isLogged: () => {
        if (clientStorage.userInfo.Email !== "") {
            console.log(clientStorage.userInfo.Email);
            return true;
        }
        return false;
    },
    clear: () => {
        localStorage.clear();
    },
}

export default clientStorage;