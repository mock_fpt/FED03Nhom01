import Config from "config";

const NumberShowDate = Config.NumberShowDate;

var days = ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'];

const currentDate = new Date();
const currentDateString = currentDate.toLocaleDateString("en-GB");

function addDays(date : string, days : number) {
    var result = new Date(Number(date.split('/')[2]), Number(date.split('/')[1]) - 1, Number(date.split('/')[0]));
    result.setDate(result.getDate() + days);
    return result.toLocaleDateString("en-GB");
}

function convertStringToDate(date: string) {
    return new Date(Number(date.split('/')[2]), Number(date.split('/')[1]) - 1, Number(date.split('/')[0]));
}

const listDateString = [currentDateString];
for(let i = 1; i < NumberShowDate; i++) {
    listDateString.push(addDays(currentDateString, i));
}

const CommonDate = {
    days: days,
    currentDateObj: currentDate,
    currentDateSring: currentDateString,
    listDateString: listDateString,
    funcConvertStringToDate: convertStringToDate
}

export default CommonDate;