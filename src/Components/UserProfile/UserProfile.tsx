import Footer from 'Components/UI/Footer/Footer'
import Header from 'Components/UI/Header/Header'
import React, { useState, useEffect } from 'react'
import './UserProfile.scss'
import useCookies from 'react-cookie/cjs/useCookies';
import APIs from 'service/APIs';
import { toast, ToastContainer } from 'react-toastify';
import { useLocation, useNavigate } from 'react-router-dom';
import Config from 'config';

interface eCurrentUser {
    fullName: string,
    urlAvatar: string,
    date: {
        dd: number,
        mm: number,
        yyyy: number,
    },
    gender: string,
    phone: string,
    address: string,
    mail: string,
    social: {
        face: string,
        twitter: string,
        instagram: string
    }
}

const lstInfoUser = [
    {
        fullName: '',
        urlAvatar: 'https://scontent.fsgn2-3.fna.fbcdn.net/v/t39.30808-1/324191497_3313049675675902_983918043262196139_n.jpg?stp=cp6_dst-jpg_p240x240&_nc_cat=107&ccb=1-7&_nc_sid=7206a8&_nc_ohc=0YuQVJo4e8QAX_ObXZk&_nc_ht=scontent.fsgn2-3.fna&oh=00_AfDHmWY_LGdCvJPNn6iGPyqaEcZJtLNDyhktBpKwM6wdJw&oe=63F233FE',
        date: {
            dd: 26,
            mm: 2,
            yyyy: 2000
        },
        gender: 'male',
        phone: '+84 793506177',
        address: '76 Đường số 7, Linh Trung, Thủ Đức',
        mail: 'ngkhang@gmail.com',
        social: {
            face: '',
            twitter: '',
            instagram: ''
        }
    },
    {
        fullName: '',
        urlAvatar: 'https://scontent.fsgn3-1.fna.fbcdn.net/v/t39.30808-1/323898165_882647582757337_7225797171688315796_n.jpg?stp=cp6_dst-jpg_p240x240&_nc_cat=111&ccb=1-7&_nc_sid=7206a8&_nc_ohc=AsAMNIxJgR0AX8i03b9&_nc_ht=scontent.fsgn3-1.fna&oh=00_AfCJ8bKlw6wKnZAc_HlelfluEV8bWuXOE6xL-nHUi1ydnQ&oe=63F4C0AA',
        date: {
            dd: 24,
            mm: 11,
            yyyy: 2001
        },
        gender: 'male',
        phone: '+84 334674390',
        address: '76 Đường số 7, Linh Trung, Thủ Đức',
        mail: 'trungnt2411@gmail.com',
        social: {
            face: '',
            twitter: '',
            instagram: ''
        }
    }
]


const userDefault = {
    fullName: '',
    urlAvatar: 'https://scontent.fsgn3-1.fna.fbcdn.net/v/t39.30808-1/323898165_882647582757337_7225797171688315796_n.jpg?stp=cp6_dst-jpg_p240x240&_nc_cat=111&ccb=1-7&_nc_sid=7206a8&_nc_ohc=AsAMNIxJgR0AX8i03b9&_nc_ht=scontent.fsgn3-1.fna&oh=00_AfCJ8bKlw6wKnZAc_HlelfluEV8bWuXOE6xL-nHUi1ydnQ&oe=63F4C0AA',
    date: {
        dd: 24,
        mm: 11,
        yyyy: 2001
    },
    gender: 'male',
    phone: '+84 334674390',
    address: '76 Đường số 7, Linh Trung, Thủ Đức',
    mail: 'trungnt2411@gmail.com',
    social: {
        face: '',
        twitter: '',
        instagram: ''
    }
}

const initialValues = {
    "current_password": "",
    "new_password": "",
    "retype_password": ""
};

interface eMyTicket {
    "Id": number,
    "ShowCode": string,
    "Email": string,
    "CinemaName": string,
    "TheaterName": string,
    "FilmName": string,
    "ImageLandscape": string,
    "ImagePortrait": string,
    "Combo": string,
    "SeatCode": string,
    "ShowTime": string
}

function UserProfile() {
    const navigate = useNavigate();
    const { pathname } = useLocation();


    const [myTickets, setMyTickets] = useState<Array<eMyTicket>>([]);
    const [openChangePass, setOpenChangePass] = useState(false);
    const [currentUser, setCurrentUser] = useState<eCurrentUser>()
    const [cookies, setCookies, removeCookies] = useCookies(['userInfo']);


    const [values, setValues] = useState(initialValues);

    useEffect(() => {
        lstInfoUser.forEach(ele => {
            if (ele.mail === cookies.userInfo.Email) {
                setCurrentUser(ele);
            } else {
                setCurrentUser(userDefault);
            }
        })

        fetch(APIs.GET_TICKET_BY_EMAIL(cookies.userInfo.Email))
            .then(res => res.json())
            .then(result => {
                setMyTickets(result);
            })


    }, [])


    useEffect(() => {
        console.log(myTickets);
    }, [myTickets])

    const handleInputChange = (e: React.KeyboardEvent<HTMLInputElement>) => {
        const { name, value } = e.currentTarget;
        setValues({
            ...values,
            [name]: value,
        });
    };

    const handlerClickChangePassword = () => {


        if (values.new_password !== values.retype_password) {
            toast(`Mật khẩu mới không khớp với nhau!`, { type: 'error', autoClose: Config.durationToast });
            return;
        }

        fetch(APIs.CHANGE_PASSWORD, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify({
                "Email": cookies.userInfo.Email,
                "Password": values.current_password,
                "PasswordNew": values.new_password
            })
        })
            .then((res) => {
                if (res.status === 200) {
                    removeCookies('userInfo', { path: '/' });
                    navigate("/account/login", {
                        state: {
                            toast: "Bạn đã đổi mật khẩu thành công. Vui lòng đăng nhập lại",
                            prevPage: pathname
                        }
                    });
                } else {
                    toast(`Đổi mật khẩu thất bại. Mật khẩu cũ không đúng!`, { type: 'warning', autoClose: Config.durationToast });
                }
            })
    }

    const handleLogout = () => {
        removeCookies('userInfo', { path: '/' });
        navigate('/');
    }

    console.log(myTickets);
    return (
        <>
            <Header />
            {
                cookies.userInfo && currentUser && <>
                    <div className='user-profile d-flex justify-content-center'>
                        <div className="mainSize">
                            <div className="header-profile">
                                <div className="banner position-relative">
                                    <i className="border-rad-8 fa-solid fa-camera-retro position-absolute fs-20 p-10" title='Change background'></i>
                                </div>
                                <div className="title position-relative py-30 pl-30 border-box d-flex justify-content-between align-items-center">
                                    <div className="avatar position-absolute">
                                        <img className='w-100 border-circle ' src={`${currentUser.urlAvatar}`} alt="avatar" />
                                    </div>
                                    <div className='info px-20 border-box d-inline-block'>
                                        <span>Hello!</span><h2>{[...cookies.userInfo?.Name.split(' ')].pop()}</h2>
                                    </div>
                                    <button className='mr-40 border-0 px-30 py-10 border-rad-8' onClick={() => handleLogout()}><i className="fa fa-sign-out" aria-hidden="true"></i> Log out</button>
                                </div>
                            </div>

                            <div className="main-profile">
                                <h3 className='fs-30 my-20'>Personal Details</h3>
                                <div className='d-flex'>
                                    <div className='w-50 mx-10'>
                                        <div className="item border-box mb-20 py-15 border-rad-8">
                                            <h4 className='fw-bold fs-20 '>Contact Info</h4>
                                            <div className="row px-20 my-15">
                                                <div className="w-100">
                                                    <p className='mb-5'>Fullname</p>
                                                    <input className='w-100' type="text" value={cookies.userInfo?.Name} />
                                                </div>
                                            </div>
                                            <div className="row px-20 my-15">
                                                <div className="w-50 d-inline-block">
                                                    <p className='mb-5'>Date</p>
                                                    <div className='d-flex align-items-center'>
                                                        <div>
                                                            <input className="numbers border-0 text-center w-100" data-bound="dd_mock" data-def="00" type="number" min="1" max="31" step="1" placeholder="DD" value={currentUser.date.dd} />
                                                        </div>
                                                        <span className="mx-5">/</span>
                                                        <div>
                                                            <input className="numbers border-0 text-center w-100" data-bound="mm_mock" data-def="00" type="number" min="1" max="12" step="1" placeholder="MM" value={currentUser.date.mm} />
                                                        </div>
                                                        <span className="mx-5">/</span>
                                                        <div>
                                                            <input className="numbers border-0 text-center w-100" data-bound="yy_mock" data-def="01" type="number" min="1900" max="2900" step="1" placeholder="YYYY" value={currentUser.date.yyyy} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="w-50 d-inline-block">
                                                    <p className='mb-5'>Gender</p>
                                                    <div className='d-flex align-items-center'>
                                                        <div className='mx-5'>
                                                            <input className='mr-5' type="radio" name='gender' id='male' value='male' />
                                                            <label htmlFor="male">Male</label>
                                                        </div>
                                                        <div className='mx-5'>
                                                            <input className='mr-5' type="radio" name='gender' id='female' value='female' />
                                                            <label htmlFor="female">Female</label>
                                                        </div>
                                                        <div className='mx-5'>
                                                            <input className='mr-5' type="radio" name='gender' id='other' value='other' />
                                                            <label htmlFor="other">Other</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row px-20 my-15">
                                                <div className="w-50 d-inline-block">
                                                    <p className='mb-5'>Phone numbers</p>
                                                    <input type="tel" placeholder='+84 000000000' value={currentUser.phone} />
                                                </div>

                                            </div>
                                            <div className="row px-20 my-15">
                                                <div className="w-100">
                                                    <p className='mb-5'>Address</p>
                                                    <textarea className='w-100 border-box' value={currentUser.address}></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="item border-box mb-20 py-15 border-rad-8">
                                            <h4 className='fw-bold fs-20'>Social Profiles</h4>
                                            <div className="row px-20 mt-15 mb-20 icon-social">
                                                <span className='p-10 border-rad-8 border-box d-inline-block mx-10'><i className="fa-brands fa-facebook fs-25 "></i></span>
                                                <span className='p-10 border-rad-8 border-box d-inline-block mx-10'><i className="fa-brands fa-instagram fab fs-25"></i></span>
                                                <span className='p-10 border-rad-8 border-box d-inline-block mx-10'>
                                                    <i className="fa-brands fa-twitter fs-25"></i>
                                                </span>
                                            </div>
                                            <h4 className='fw-bold fs-20'>Notification</h4>
                                            <div className="row px-20 my-15">
                                                <div className="d-flex align-items-center justify-content-between mb-5">
                                                    <p className='d-inline-block'>Following the news and offers</p>
                                                    <i className="fa-solid fa-arrow-right-long"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='w-50 mx-10'>
                                        <div className="item border-box mb-20 py-15 border-rad-8">
                                            <h4 className='fw-bold fs-20'>Account</h4>
                                            <div className="row px-20 my-15">
                                                <div className="w-100">
                                                    <p className='mb-5'>Email address</p>
                                                    <div className='position-relative'>
                                                        <input disabled className='w-100' type="email" placeholder='abc@.com' value={cookies.userInfo?.Email} />
                                                        <i style={{ top: '15px', right: '15px' }} className="fa-solid fa-at position-absolute"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row px-20 my-15">
                                                <p className='mb-5'>Password</p>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <input className='' type="password" placeholder='1234' value='1234' disabled />
                                                    <button className='btnOpenChangePass p-10 border-rad-8' onClick={() => setOpenChangePass(!openChangePass)}>Change password</button>
                                                </div>
                                            </div>
                                            {
                                                openChangePass && <>
                                                    <div className="px-30 my-15">
                                                        <p className='fs-14'><i className="fa-solid fa-shield-halved"></i> What makes a strong password?</p>
                                                        <div className='d-flex align-items-center my-5'>
                                                            <p className='text-end mr-10' style={{ flex: '1 1 0' }}>Current password</p>
                                                            <input onKeyUp={handleInputChange} name='current_password' className='' type="password" placeholder='*********' />
                                                        </div>
                                                        <div className='d-flex align-items-center my-5'>
                                                            <p className='text-end mr-10' style={{ flex: '1 1 0' }}>New password</p>
                                                            <input onKeyUp={handleInputChange} name='new_password' className='' type="password" placeholder='*********' />
                                                        </div>
                                                        <div className='d-flex align-items-center my-5'>
                                                            <p className='text-end mr-10' style={{ flex: '1 1 0' }}>Retype password</p>
                                                            <input onKeyUp={handleInputChange} name='retype_password' className='' type="password" placeholder='*********' />
                                                        </div>
                                                        <div className='row'>
                                                            <p className='mb-5 fs-14'><input type='checkbox' /> Require all devices to sign in with new password</p>
                                                            <div className="text-end">
                                                                <button className='btnDisChange py-10 px-15 border-rad-8 mr-15' onClick={() => setOpenChangePass(false)}>Dischange</button>
                                                                <button onClick={handlerClickChangePassword} className='btnChangePass py-10 px-15 border-rad-8'>Change password</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                            }

                                        </div>

                                        <div className="item border-box mb-20 py-15 border-rad-8">
                                            <h4 className='fw-bold fs-20'>Security</h4>
                                            <div className="row px-20 my-15">
                                                <div className="d-flex align-items-center justify-content-between mb-5">
                                                    <p className='d-inline-block'>Devices that remember your password</p>
                                                    <i className="fa-solid fa-arrow-right-long"></i>
                                                </div>
                                                <div className="d-flex align-items-center justify-content-between mb-5">
                                                    <p className='d-inline-block'>Where you're signed in</p>
                                                    <i className="fa-solid fa-arrow-right-long"></i>
                                                </div>
                                                <div className="d-flex align-items-center justify-content-between mb-5">
                                                    <p className='d-inline-block'>Two-step verification</p>
                                                    <span>Off<i className="ml-10 fa-solid fa-arrow-right-long"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h3 className='fs-30 my-20'>My Ticket</h3>
                                <div className="item border-box mb-40 p-15 border-rad-8">
                                    <div className="lst-items-ordered">
                                        <table className='w-100 border-box' style={{ borderCollapse: 'collapse' }}>
                                            <thead>
                                                <tr>
                                                    <th style={{ width: '7%' }}>#</th>
                                                    <th style={{ width: '150px' }}>Movie</th>
                                                    <th style={{ width: '40%' }}>Showtimes</th>
                                                    <th style={{ width: '15%' }}>Seat(s)</th>
                                                    <th>Concession(s)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    myTickets.map((ticket: eMyTicket, index: number) => {
                                                        return <>
                                                            <tr>
                                                                <td className='text-center'>{index + 1}</td>
                                                                <td className='border-box p-5'>
                                                                    <div className='image'>
                                                                        <img className='w-100 border-rad-8' src={ticket.ImagePortrait} alt={ticket.FilmName} />
                                                                    </div>
                                                                    <h5 className='fs-16'>{ticket.FilmName}</h5>
                                                                </td>
                                                                <td>
                                                                    <h5 className='fs-18'>{ticket.CinemaName}</h5>
                                                                    <p className='fs-16 my-5'>{ticket.TheaterName}</p>
                                                                    <p className='fs-14'>{new Date(ticket.ShowTime.replace('.000', '')).toISOString().substring(11, 16)} {new Date(ticket.ShowTime.replace('.000', '')).toISOString().substring(0, 10)}</p>
                                                                </td>
                                                                <td style={{ textAlign: 'start' }}>{ticket.SeatCode}</td>
                                                                <td>{ticket.Combo}</td>
                                                            </tr>
                                                        </>
                                                    })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div className="item border-box mb-40 px-10 py-15 border-rad-8" style={{ boxShadow: '0px 0px 6px 1px rgb(255 0 40 / 98%)' }}>
                                    <h4 className='fw-bold fs-25' style={{ color: 'rgb(223 21 82 / 98%)' }}>Danger zone</h4>
                                    <p className="px-20 my-5">Delete user account</p>
                                    <div className="row px-20 my-15">
                                        <p className='mb-5'>By deleting your account you will lose all data and access toany workspaces that you are associated with.</p>
                                        <button style={{ backgroundColor: 'rgb(223 21 82 / 98%)', color: '#fff' }} className='px-20 py-10 border-rad-8 border-0 fs-16'>Request account delation</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </>
            }
            <Footer />
        </>
    )
}

export default UserProfile