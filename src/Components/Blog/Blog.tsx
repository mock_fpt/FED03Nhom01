import React, { useState, useEffect } from "react";
import { useParams, NavLink, Route, Routes } from "react-router-dom";

import Header from "Components/UI/Header/Header";
import Footer from "Components/UI/Footer/Footer";
import "./Blog.scss";
import CardBlog from "./CardBlog/CardBlog";

import eBlog, { eContent } from "./eBlog";
import fakeApiBlog from "./fakeApiBlog/fakeApiBlog";

const NavItems = [
    {
        type: "Mới nhất",
        url: "",
    },
    {
        type: "Phim chiếu rạp",
        url: "phim-chieu-rap",
    },
    {
        type: "Review phim",
        url: "review",
    },
    {
        type: "Top phim",
        url: "top-phim",
    },
    {
        type: "Netflix",
        url: "netflix",
    },
];

const CategoryDefault = {
    Name: "Blog Điện Ảnh",
    Description:
        "Theo dõi các bộ sưu tập bao gồm Review phim - Top phim hay của Ví MoMo luôn được cập nhật tại đây!",
};

function Blog() {
    const urlParam = useParams();
    const [blog, setDataBlog] = useState<eBlog>({
        Title: {
            Name: CategoryDefault.Name,
            Description: CategoryDefault.Description,
        },
    });
    const getTypeContentBlog = (type = "") => {
        let rawContent: eContent;
        if (type.length === 0) {
            let tempKey = 'main-blog';
            rawContent = fakeApiBlog[tempKey].pageProps.dataBlog.Data;
            return {
                Title: {
                    Name: CategoryDefault.Name,
                    Description: CategoryDefault.Description
                },
                Content: rawContent
            }
        }
        rawContent = fakeApiBlog[type].pageProps.dataBlog.Data;
        return {
            Title: {
                Name: rawContent.Category.Name,
                Description: rawContent.Category.Description
            },
            Content: rawContent
        }
    };

    useEffect(() => {
        let dataResponse = getTypeContentBlog(urlParam["*"]);
        setDataBlog(dataResponse);
    }, [urlParam]);

    return (
        <>
            <Header />
            <div className="blog">
                <div className="preview d-flex justify-content-center">
                    <div className="mainSize">
                        <div className="title">
                            <h1 className="fs-45 py-15">{blog.Title.Name}</h1>
                            <p className="mb-10">{blog.Title.Description}</p>
                        </div>
                        {blog.Content && (
                            <>
                                <div className="nav mb-20">
                                    {NavItems.map((btnNav) => {
                                        return (
                                            <>
                                                <NavLink key={btnNav.type} to={`${btnNav.url}`} end className="d-inline-block text-decoration-none p-15">{btnNav.type}
                                                </NavLink>
                                            </>
                                        );
                                    })}
                                </div>
                                <Routes>
                                    <Route path={`${urlParam["*"]}`} element={<CardBlog data={blog.Content} />} />
                                </Routes>
                            </>
                        )}
                    </div>
                </div>
            </div>
            <Footer />
        </>
    );
}

export default Blog;