import MainBlog from './blog-dien-anh.json'
import Netflix from './blog-netflix.json'
import Review from './blog-review-phim.json'
import TopMovie from './blog-top-phim.json'
import MovieBlog from './blog-phim-chieu-rap.json';

const fakeApiBlog: { [key: string]: any } = {
    'main-blog': MainBlog,
    'phim-chieu-rap': MovieBlog,
    'review': Review,
    'top-phim': TopMovie,
    'netflix': Netflix
}
export default fakeApiBlog;