interface eDetailBlog {
    Title: string,
    CategoryChildName: string,
    ShortContent: string,
    Avatar: string,
    TotalViewsFormat: string,
}

export interface eTitle {
    Name: string,
    Description: string,
}

export interface eContent {
    Category: {
        Name: string,
        Description: string
    },
    ListBlogs: {
        Items: Array<eDetailBlog>
    },
    ListBlogFeatured: {
        Items: Array<eDetailBlog>
    },
    ListBlogTopView: {
        Items: Array<eDetailBlog>
    }
}

interface eBlog {
    Title: eTitle,
    Content?: eContent
}

export default eBlog;