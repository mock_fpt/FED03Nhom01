import React from 'react'
import './CardBlog.scss'
import { eContent } from '../eBlog'

function CardBlog(props: { data: eContent }) {
    return (
        <div className='main mb-20'>
            {
                props.data.ListBlogFeatured.Items && <>
                    <div className="feature-blog d-flex justify-content-center flex-wrap pb-10">
                        {
                            props.data.ListBlogFeatured.Items.map((e, i: number) => {
                                return <>
                                    <div key={`featureItem${i + 1}`} className="border-box px-10 my-10">
                                        <img className='w-100' src={`${e.Avatar}`} alt="poster blog" />
                                        <p className='fs-14 fw-bolder'>{e.CategoryChildName}</p>
                                        <p className='fs-18 fw-bold my-10'>{e.Title}</p>
                                        <p className='fs-12 fw-bolder mb-5'><i className="fa-regular fa-eye"></i> {e.TotalViewsFormat + ""} lượt xem</p>
                                        <p className='fs-14 short-desc'>{e.ShortContent}</p>
                                    </div>
                                </>
                            })
                        }
                    </div>
                    <div className="content d-flex">
                        <div className="list-post mr-40">
                            <h4 className='fs-18 mt-15'>Bài viết mới nhất</h4>
                            {
                                props.data.ListBlogs.Items.map((e, i: number) => {
                                    return <>
                                        <div key={`blogItem${i + 1}`} className="py-15 d-flex">
                                            <div className="mr-10 border-box">
                                                <img className='border-rad-8' src={`${e['Avatar']}`} alt="poster blog" />
                                            </div>
                                            <div className="flex-grow-1">
                                                <h4 className='fs-18'>{e.Title}</h4>
                                                <p className='fs-14 my-5'><i className="fa-regular fa-eye"></i> {e.TotalViewsFormat + ""} lượt xem</p>
                                                <p className='fs-14 short-desc'>{e.ShortContent}</p>
                                            </div>
                                        </div>
                                    </>
                                })
                            }
                        </div>
                        <div className="post-topview">
                            <h4 className='fs-18 mt-15'><i className="fa-solid fa-arrow-trend-up"></i> Xem nhiều nhất</h4>
                            {
                                props.data.ListBlogTopView.Items.map((e, i: number) => {
                                    return <>
                                        <div key={`topViewItem${i + 1}`} className="pt-15 border-box d-flex">
                                            <div className="mr-5 poster">
                                                <div className='position-relative h-100'>
                                                    <img className='border-rad-8 h-100' src={`${e['Avatar']}`} alt="poster blog" />
                                                    <span className='position-absolute fs-30 fw-bold'>{(i > 9 ? i + 1 : `0${i + 1}`)}</span>
                                                </div>
                                            </div>
                                            <div>
                                                <p className='line-header fs-12 fw-bold mb-5'>{e.CategoryChildName}</p>
                                                <p className='title fs-14 fw-bold'>{e.Title}</p>
                                            </div>
                                        </div>
                                    </>
                                })
                            }
                        </div>
                    </div>
                </>
            }
        </div>
    )
}

export default CardBlog