import reduxSaga from "redux-saga";
import MiddleReSa from "./Saga/MiddleReSa";

import rdcAllMovies from "./Reducer/rdcAllMovies";
import rdcAllCinema from "./Reducer/rdcAllCinema";
const redux = require("redux");
const middleware = reduxSaga();

const globalState = {
    rdcAllMovies: rdcAllMovies,
    rdcAllCinema: rdcAllCinema,
}

const allReducer = redux.combineReducers(globalState);

export default redux.createStore(
    allReducer,
    redux.applyMiddleware(middleware)
);

middleware.run(MiddleReSa);
