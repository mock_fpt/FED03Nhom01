import Action from "../Action/Action";
import eCinema from 'Components/Interface/eCinema';

interface Dispatch {
    type: string,
    payload: {
        statusApi: string | number,
        listCinema: Array<eCinema>,
    }
}
const initialState = {
    statusApi: null,
    data: [],
}

const rdcAllCinema = (state = initialState, { type, payload }: Dispatch) => {
    switch (type) {
        case Action.GET_LIST_ALL_CINEMA:
            return {
                statusApi: payload.statusApi,
                data: payload.listCinema
            };
        default:
            return state;
    }
}
export default rdcAllCinema;