import Action from "../Action/Action";

interface Dispatch {
    type: string,
    payload: {
        statusApi: string | number,
        data: {
            movieCommingSoon: [],
            movieShowing: [],
        }
    }
}
const initialState = {
    statusApi: null,
    lstNowShowing: [],
    lstComingSoon: [],
}

const rdcAllMovies = (state = initialState, { type, payload }: Dispatch) => {
    switch (type) {
        case Action.GET_LIST_ALL_MOVIE:
            return {
                statusApi: payload.statusApi,
                lstNowShowing: payload.data.movieShowing,
                lstComingSoon: payload.data.movieCommingSoon,
            };
        default:
            return state;
    }
}
export default rdcAllMovies;