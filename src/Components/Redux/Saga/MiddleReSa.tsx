import { takeEvery, call, put } from "redux-saga/effects";
import Action from "../Action/Action";
import APIs from "../../../service/APIs"



const API_LIST_ALL_MOVIE = async () => {
    let res = await fetch(APIs.GET_ALL_MOVIES);
    let rawData = await res.json();
    return {
        statusApi: res.status,
        data: rawData
    }
}

const API_LIST_ALL_CINEMA = async () => {
    let res = await fetch(APIs.GET_ALL_CINEMAS);
    let rawData = await res.json();
    return {
        statusApi: res.status,
        listCinema: rawData
    }
}

// Function Fetch Api All Movies
function* fetch_list_all_movies(): any {
    console.log("API FETCH ALL MOVIE: START");
    let data = yield call(API_LIST_ALL_MOVIE);
    console.log("API FETCH ALL MOVIE: END");
    yield put({
        type: Action.GET_LIST_ALL_MOVIE,
        payload: data
    })
}

// Function Fetch Api All Cinema
function* fetch_list_all_cinema(): any {
    console.log("API FETCH ALL CINEMA: START");
    let data = yield call(API_LIST_ALL_CINEMA);
    console.log("API FETCH ALL CINEMA: END");
    yield put({
        type: Action.GET_LIST_ALL_CINEMA,
        payload: data
    })
}
function* mySaga() {
    yield takeEvery(Action.FETCH_LIST_ALL_MOVIES, fetch_list_all_movies)
    yield takeEvery(Action.FETCH_LIST_ALL_CINEMA, fetch_list_all_cinema)
}

export default mySaga;