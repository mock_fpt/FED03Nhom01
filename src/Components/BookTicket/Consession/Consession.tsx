import { eSeats } from 'Components/Interface/eBooking';
import eCinema from 'Components/Interface/eCinema';
import { eFilm } from 'Components/Interface/eCineplexDetail';
import eConsession from 'Components/Interface/eConsession';
import NoData from 'Components/UI/PagesError/NoData/NoData';
import React, { useState, useEffect } from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom';
import APIs from 'service/APIs';
import useCookies from 'react-cookie/cjs/useCookies';

import './Consession.scss'
var CurrencyFormat = require('react-currency-format');

interface eProps {
    setOpenFunc: (isOpen: boolean) => void,
    cineplex?: any,
    session?: any,
    film?: eFilm,
    listSelectSeatsString?: Array<string>,
    listSelectSeats?: Array<eSeats>,
    totalPriceSeats?: number
}


function Consession(props: eProps) {
    const navigate = useNavigate();
    const {pathname} = useLocation();

    const [cookies, setCookies] = useCookies(['userInfo']);

    const [lstItemsChossed, setlstItemsChossed] = useState<Array<eConsession>>([]);
    const [totalPriceCombo, setTotalPriceCombo] = useState(0);

    useEffect(() => {
        fetch(APIs.GET_BOOKING_DETAILS)
            .then(res => res.json())
            .then(data => {
                setlstItemsChossed(data.consession[0].concessionItems);
            })
    }, []);

    const handleCountItem = ({ key, info }: { key: string, info: eConsession }) => {
        let index = lstItemsChossed.indexOf(info)
        var stateTemp = [...lstItemsChossed];
        if (index >= 0) {
            switch (key) {
                case 'up':
                    stateTemp[index] = {
                        ...stateTemp[index],
                        defaultQuantity: stateTemp[index].defaultQuantity + 1,
                    }
                    break;
                default:
                    stateTemp[index] = {
                        ...stateTemp[index],
                        defaultQuantity: stateTemp[index].defaultQuantity - 1,
                    }
                    break;
            }
            setlstItemsChossed(stateTemp);
        }
    }

    useEffect(() => {
        let total = 0;
        lstItemsChossed.filter((item) => total += item.defaultQuantity * item.displayPrice)
        setTotalPriceCombo(total);
        console.log(lstItemsChossed, total, totalPriceCombo)
    }, [lstItemsChossed])

    const handlerClickContinue = () => {

        let state = {
            cineplex: props.cineplex,
            film: props.film,
            session: props.session,
            lstItemsChossed: lstItemsChossed,
            listSelectSeats: props.listSelectSeats,
            listSelectSeatsString: props.listSelectSeatsString,
            totalPriceSeats: props.totalPriceSeats,
            totalPriceCombo: totalPriceCombo
        }

        // Check Logged
        if(!cookies.userInfo) {
            navigate("/account/login", {
                state: {
                    ...state,
                    toast: "Vui lòng đăng nhập trước.",
                    isGotoPayment: true
                }
            });
            return;
        }

        navigate('/booking/payment', {
            state: state
        });
    }

    return (
        <div className="consession d-flex align-items-center justify-content-center py-40 border-box" onClick={() => props.setOpenFunc(false)}>
            <div className='main border-rad-15 position-relative d-flex flex-col' onClick={(event) => event.stopPropagation()}>
                <div className="header-consession py-20 text-center">
                    <h3 className='fs-25'>Combo - Bắp nước</h3>
                </div>
                <div className="list-consession">
                    {
                        lstItemsChossed.length > 0
                            ? lstItemsChossed.map(e => {
                                return <>
                                    <div key={e.id} className="item d-flex align-items-center border-box px-20 py-15">
                                        <div className='imgItem mr-15'>
                                            <img className='border-box w-100' src={`${e.imageUrl}`} alt="poster movie" />
                                        </div>
                                        <div className="content">
                                            <div className="info">
                                                <h5 className='fs-20 my-5'>{e.description} - <CurrencyFormat value={e.displayPrice} displayType={'text'} thousandSeparator={true} suffix={'đ'} /></h5>
                                                <p className='fs-14 mb-10'>
                                                    {e.extendedDescription}
                                                </p>
                                            </div>
                                            <div className="counter d-flex align-items-center">
                                                <button disabled={e.defaultQuantity === 0 && true} className='down mx-5' onClick={() => handleCountItem({ key: 'down', info: e })}>
                                                    <i className="fa-2x fa-solid fa-circle-minus"></i>
                                                </button>
                                                <span className='mx-5 px-10 py-5 text-center fs-18 fw-bold'>{e.defaultQuantity}</span>
                                                <button className='up mx-5' onClick={() => handleCountItem({ key: `up`, info: e })}>
                                                    <i className="fa-2x fa-solid fa-circle-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </>
                            })
                            : <NoData sizeSvg={{ width: 120, height: 120 }} content={{ title: 'Chúng mình tạm hết rồi bạn nhé', sub: 'Xin lỗi bạn nhiều nhaaaa' }} />
                    }
                </div>
                <div className="footer-consesion px-20 pb-20 border-box">
                    <div className="d-flex align-items-center justify-content-between my-10">
                        <span className='fs-20'>Tổng cộng</span>
                        <span className='fs-25 fw-bold'><CurrencyFormat value={totalPriceCombo} displayType={'text'} thousandSeparator={true} suffix={'đ'} /></span>
                    </div>
                    <div onClick={handlerClickContinue} className='btn text-decoration-none d-inline-block py-15 w-100 border-0 border-box fs-20 border-rad-8 text-center'>
                        Tiếp tục
                    </div>
                    {/* <Link to={'/booking/payment'} className='text-decoration-none d-inline-block py-15 w-100 border-0 border-box fs-20 border-rad-8 text-center'>
                        Tiếp tục
                    </Link> */}
                </div>
            </div>
        </div>
    )
}

export default Consession;