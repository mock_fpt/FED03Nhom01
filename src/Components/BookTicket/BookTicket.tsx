import Footer from 'Components/UI/Footer/Footer'
import Header from 'Components/UI/Header/Header'
import React, { useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import './BookTicket.scss'
import Consession from './Consession/Consession'

function BookTicket() {
    const ticket = useLocation();
    const [isOpen, setIsOpen] = useState(false);

    return (<>
        <Header />


        <button onClick={() => { setIsOpen(true) }
        }> Open</button >

        {
            isOpen && <Consession setOpenFunc={setIsOpen} />
        }
        {/* <p> {JSON.stringify(ticket.state)}</p > */}
        <Footer />
    </>
    )
}

export default BookTicket