import React, { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import APIs from 'service/APIs';
import { eAreas, eRows, eSeats } from 'Components/Interface/eBooking';

import 'react-toastify/dist/ReactToastify.css';
import './ModalBookingTicket.scss';
import Consession from 'Components/BookTicket/Consession/Consession';
import Config from 'config';
import Header from 'Components/UI/Header/Header';
import Footer from 'Components/UI/Footer/Footer';

const CurrencyFormat = require('react-currency-format');

export default function ModalBookingTicket() {
  const { state } = useLocation();
  const [areas, setAreas] = useState<Array<eAreas>>([]);
  const [seatsSoldOut, setSeatsSoldOut] = useState<Array<string>>([]);
  const [listSelectSeats, setListSelectSeats] = useState<Array<eSeats>>([]);
  const [listSelectSeatsString, setListSelectSeatsString] = useState<Array<string>>([]);
  const [isOpenModal, setIsOpenModal] = useState(false);

  const [totalPriceSeats, setTotalPriceSeats] = useState(0);

  useEffect(() => {
    console.log(seatsSoldOut);
  }, [seatsSoldOut])

  useEffect(() => {
    console.log(state.film, state.session, state.tickets, state.cineplex)
    fetch(APIs.GET_BOOKING_DETAILS)
      .then(res => res.json())
      .then(data => {
        setAreas(reverseRowsOnAreas(data.seatPlan.seatLayoutData.areas));
      });

    fetch(APIs.GET_TICKET_BY_SHOWCODE(state.session.id))
      .then(res => res.json())
      .then(data => {
        data.forEach((seat: any) => {
          let arr = seat.SeatCode.split(', ');
          setSeatsSoldOut(current => [...current, ...arr]);
        })
      })

  }, []);

  useEffect(() => {
    if (listSelectSeats.length > Config.maxBookingSeats) {
      setListSelectSeats(listSelectSeats.slice(0, -1));
      setListSelectSeatsString(listSelectSeatsString.slice(0, -1));
      toast(`Không được quá ${Config.maxBookingSeats} ghế`, { type: 'warning', autoClose: Config.durationToast });
    }
    UpdatePriceSeats();
  }, [listSelectSeats])

  const reverseRowsOnAreas = (arrAreas: Array<eAreas>) => {
    for (let i = 0; i < arrAreas.length; i++) {
      arrAreas[i].rows = arrAreas[i].rows.reverse();
    }
    return arrAreas;
  }

  const getAreaOfSeats = (seats: eSeats): any => {
    for (let i = 0; i < areas.length; i++) {
      if (seats.position.areaNumber === areas[i].number) {
        return areas[i];
      }
    }
  }

  const getPriceWithArea = (area: eAreas) => {
    let areaCodeCategory = area.areaCategoryCode;
    for (let i = 0; i < state.tickets.length; i++) {
      if (state.tickets[i].areaCategoryCode === areaCodeCategory) {
        return state.tickets[i].displayPrice;
      }
    }
  }

  const UpdatePriceSeats = () => {
    let totalPrice = 0;
    listSelectSeats.forEach((selectedSeats) => {
      totalPrice += getPriceWithArea(getAreaOfSeats(selectedSeats));
    })
    setTotalPriceSeats(totalPrice);
  }

  const selectSeats = (seats: Array<eSeats>, area: eAreas, row: eRows) => {



    let listSelectSeatsTemp = [...listSelectSeats];
    let listSelectSeatsStringTemp = [...listSelectSeatsString];


    let isUpdate = false;
    seats.forEach((seat: eSeats) => {
      let seatsName = row.physicalName + seat.id;

      if (seatsSoldOut.includes(seatsName)) {
        return;
      }
      if (listSelectSeatsTemp.includes(seat)) {
        isUpdate = true;
        // remove seats
        let indexOfSeats = listSelectSeatsTemp.indexOf(seat);
        let tempArrSeats = [...listSelectSeatsTemp];
        tempArrSeats.splice(indexOfSeats, 1);
        listSelectSeatsTemp = [...tempArrSeats];

        // remove seats name
        let indexOfSeatsName = listSelectSeatsStringTemp.indexOf(seatsName);
        let tempArrSeatsName = [...listSelectSeatsStringTemp];
        tempArrSeatsName.splice(indexOfSeatsName, 1);
        listSelectSeatsStringTemp = [...tempArrSeatsName];
      } else {
        setListSelectSeats(current => [...current, seat]);
        setListSelectSeatsString(current => [...current, seatsName]);
      }
    })

    if (isUpdate) {
      setListSelectSeats(listSelectSeatsTemp);
      setListSelectSeatsString(listSelectSeatsStringTemp);
    }
  }

  const clearSelectedSeats = () => {
    setListSelectSeatsString([]);
    setListSelectSeats([]);
  }

  const handlerBuyTicket = () => {
    if (listSelectSeats.length <= 0) {
      toast("Vui lòng chọn ít nhất 1 ghế!!!", { type: "warning", position: "top-center", closeButton: false, autoClose: Config.durationToast })
    } else {
      setIsOpenModal(true);
    }
  }

  return (<>
    <Header />
    <div className='modal-booking-ticket d-flex flex-col align-items-center mb-40 mt-20'>
      <div className="border-box">
        <h3 className='text-center py-15'>Mua vé xem phim</h3>
        <div className="center">
          <div className="screen pt-20 mb-10">
            <p className='py-5 text-center w-50 fs-12'>MÀN HÌNH</p>
          </div>
          <div className="seats border-box mb-30">
            <div className="seats-list">
              {
                areas.map((area) => {
                  if (area.areaCategoryCode === "0000000002") {
                    return area.rows.map((row, rowIndex) => {
                      return <div key={rowIndex} className="seats-row d-flex justify-content-center mb-5 text-center">
                        {
                          row.seats.map((seat, seatIndex) => {
                            return <div onClick={(e) => selectSeats([seat], area, row)} key={seatIndex} className={"seats-item fs-12 p-5 border-box standard " + (listSelectSeats.includes(seat) ? " selected " : "") + (seatsSoldOut.includes(row.physicalName + seat.id) ? " sold-out " : "")}>{row.physicalName + seat.id}</div>
                          })
                        }
                      </div>
                    })
                  } else {
                    return area.rows.map((row, rowIndex) => {
                      return <div key={rowIndex} className="seats-row d-flex justify-content-center mb-5 text-center">
                        {
                          row.seats.map((seat, seatIndex, element) => {
                            if (seatIndex % 2 === 0) {
                              return <div className='couple-seats' onClick={(e) => selectSeats([seat, element[seatIndex + 1]], area, row)}>
                                <div className={"seats-item fs-12 p-5 border-box couple " + (listSelectSeats.includes(seat) ? "selected" : "") + (seatsSoldOut.includes(row.physicalName + seat.id) ? " sold-out " : "")}>{row.physicalName + seat.id}</div>
                                <div className={"seats-item fs-12 p-5 border-box couple " + (listSelectSeats.includes(seat) ? "selected" : "") + (seatsSoldOut.includes(row.physicalName + seat.id) ? " sold-out " : "")}>{row.physicalName + element[seatIndex + 1].id}</div>
                              </div>
                            }
                          })
                        }
                      </div>
                    })
                  }
                })
              }
            </div>
          </div>
          <div className="seats-note d-flex justify-content-center pb-20">
            <div className="note-item d-flex align-items-center mx-15">
              <div className="seats sold-out"></div>
              <p>Đã đặt</p>
            </div>
            <div className="note-item d-flex align-items-center mx-15">
              <div className="seats selected"></div>
              <p>Ghế bạn chọn</p>
            </div>
            <div className="note-item d-flex align-items-center mx-15">
              <div className="seats standard"></div>
              <p>Ghế thường</p>
            </div>
            <div className="note-item d-flex align-items-center mx-15">
              <div className="seats couple"></div>
              <p>Ghế đôi</p>
            </div>
          </div>
        </div>
        <div className="footer-ticket border-box py-10 px-20">
          <div className='film-info border-box py-10'>
            <div className='d-flex align-items-center mb-15'>
              <span className={'border-box mr-10 fs-12 border-0 ' + (state.film.age === "0" ? "age-p" : state.film.age === "13" ? "age-13" : state.film.age === "16" ? "age-16" : state.film.age === "18" ? "age-18" : "")}>{state.film.age !== "0" ? `${state.film.age}+` : "P"}</span>
              <p className='name fw-bold fs-18'>{state.film.name}</p>
            </div>
            <p className='schedule fs-14'>{state.session.showTime} ~ {new Date(`01/01/1970 ${state.session.showTime}`).addHours(state.film.duration / 60).toTimeString().replace(/.*(\d{2}:\d{2}):\d{2}.*/, "$1")} · {state.session.showDate} · Phòng chiếu {state.session.screenName}</p>
          </div>
          <div className='seats py-10 border-box d-flex align-items-center justify-content-between'>
            <p>Chỗ ngồi</p>
            <div className='seat-list border-box px-10 py-5 border-rad-8' style={{ visibility: `${listSelectSeats.length > 0 ? "unset" : "hidden"}` }}>
              {listSelectSeatsString.join(', ')} <span onClick={() => clearSelectedSeats()}><i className="fa-solid fa-circle-xmark"></i></span>
            </div>
          </div>
          <div className='total-price mt-10 border-box d-flex justify-content-between align-items-center'>
            <div>
              <p className='fs-16'>Tạm tính</p>
              <h2><CurrencyFormat value={totalPriceSeats} displayType={'text'} thousandSeparator={true} suffix={'đ'} /></h2>
            </div>
            <button className='border-0 border-rad-8 fw-bold border-box px-40 py-15' onClick={() => handlerBuyTicket()}>Mua vé</button>
          </div>
        </div>
      </div>
    </div>
    {
      isOpenModal &&
      <Consession
        cineplex={state.cineplex}
        session={state.session}
        film={state.film}
        listSelectSeatsString={listSelectSeatsString}
        totalPriceSeats={totalPriceSeats}
        listSelectSeats={listSelectSeats}
        setOpenFunc={setIsOpenModal}
      />
    }
    <Footer />
  </>
  )
}
