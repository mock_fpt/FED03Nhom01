import Footer from 'Components/UI/Footer/Footer';
import Header from 'Components/UI/Header/Header';
import React, { useState, useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';
import APIs from 'service/APIs';
import CommonDate from 'Components/Common/CommonDate';

import './CineplexDetail.scss';

import { eBundles, eFilm, eTicket } from 'Components/Interface/eCineplexDetail';
import NoData from 'Components/UI/PagesError/NoData/NoData';
import ModalMapCine from 'Components/UI/ModalMapCine/ModalMapCine';

export default function CineplexDetail() {

    const navigate = useNavigate();
    const { state } = useLocation();
    const [currentDay, setCurrentDay] = useState(CommonDate.currentDateSring);
    const [films, setFilms] = useState<Array<eFilm>>([]);
    const [filmsShowingToDay, setFilmsShowingToDay] = useState<Array<eFilm>>([]);
    const [tickets, setTickets] = useState<Array<eTicket>>();
    const [isOpenMap, setIsOpenMap] = useState(false);

    useEffect(() => {
        fetch(APIs.GET_ALL_MOVIE_OF_CINEMA(state.cineplex.code))
            .then(res => res.json())
            .then(data => {
                setFilms(data);
            })

        fetch(APIs.GET_BOOKING_DETAILS)
            .then(res => res.json())
            .then(data => {
                setTickets(data.ticket);
            })
    }, [])

    useEffect(() => {
        let arrFilms: eFilm[] = [];
        for (let i = 0; i < films.length; i++) {
            if (isCheckFilmIsShowOnDay(films[i], currentDay)) {
                arrFilms.push(films[i]);
            }
        }
        setFilmsShowingToDay(arrFilms);
    }, [currentDay, films])

    const isCheckFilmIsShowOnDay = (film: eFilm, date: String): Boolean => {
        for (let i = 0; i < film.dates.length; i++) {
            if (film.dates[i].showDate === date) {
                return true;
            }
        }
        return false;
    }

    const filmSessionWithDay = (film: eFilm, date: String): Array<eBundles> => {
        let result = [];
        for (let i = 0; i < film.dates.length; i++) {
            if (film.dates[i].showDate === date) {
                for (let j = 0; j < film.dates[i].bundles.length; j++) {
                    result.push(film.dates[i].bundles[j]);
                }
            }
        }
        return result;
    }

    const selectCurrentDate = (e: React.MouseEvent<HTMLElement>): void => {
        setCurrentDay(e.currentTarget.dataset.date + "");
    }

    const handlerSelectScheduleFilm = (film: eFilm, session: any) => {
        navigate("/booking/seats", {
            state: {
                tickets: tickets,
                film: film,
                session: session,
                cineplex: state.cineplex,
            }
        });
    }

    return (
        <>
            <Header />
            <div className="cineplex-detail">
                <div className='cineplex-detail__info d-flex align-items-end justify-content-center position-relative' style={{ backgroundImage: `url('https://cdn.galaxycine.vn${state.cineplex.imageLandscape}')` }} >
                    <div className="bg-blur w-100 h-100 position-absolute"></div>
                    <div className="title mainSize d-flex mb-30">
                        <img className='mr-15 border-rad-8' src="https://static.mservice.io/cinema/momo-upload-api-211123095138-637732578984425272.png" alt="banner cinema" />
                        <div>
                            <h1 className='fs-30'>{state.cineplex.name}</h1>
                            <p>{state.cineplex.address}</p>
                            <span className='fs-16 fw-bold' onClick={() => setIsOpenMap(true)}>[Xem Bản đồ]</span>
                        </div>
                    </div>
                </div>
                <h1 className='text-center'>Lịch chiếu phim {state.cineplex.name}</h1>
                <div className='cineplex-detail__schedule p-20 border-box'>
                    <div className='date-list d-flex pb-10 mb-20'>
                        {
                            CommonDate.listDateString.map((date, dateKey) => {
                                return <div key={dateKey} onClick={selectCurrentDate} className={"date-item mr-10 " + (date === currentDay ? "active" : "")} data-date={date}>
                                    <div className='day-number py-5 border-box text-center fw-bold'>{date.split('/')[0]}</div>
                                    <div className='day-full fs-12 text-center py-5'>{CommonDate.days[CommonDate.funcConvertStringToDate(date).getDay()]}</div>
                                </div>
                            })
                        }
                    </div>
                    {
                        filmsShowingToDay.length > 0 ?
                            <div className="film-list">
                                {
                                    filmsShowingToDay.map((film, keyFilm) => {
                                        return <div key={keyFilm} className="film-item d-flex align-items-start border-box mb-20 pb-20">
                                            <div className='logo'>
                                                <img src={film.imagePortrait} alt="" className='w-100 h-100' />
                                            </div>
                                            <div className='film-info ml-20'>
                                                <span className={"age " + (film.age === "0" ? "age-p" : film.age === "13" ? "age-13" : film.age === "16" ? "age-16" : film.age === "18" ? "age-18" : "")}>{film.age !== "0" ? `${film.age}+` : "P"}</span>
                                                <div className='film-name'>{film.name}</div>
                                                <div className='film-category fs-14'>{film.subname}</div>
                                                {
                                                    filmSessionWithDay(film, currentDay).map((bundle: any) => {
                                                        return <>
                                                            <div className='mb-10'>{bundle.version}</div>
                                                            <div className='schedule-list d-flex flex-wrap'>
                                                                {bundle.sessions.map((session: any) => {
                                                                    return <>
                                                                        <div onClick={() => handlerSelectScheduleFilm(film, session)} className="schedule py-5 px-15 border-box fs-14">
                                                                            <span className='fw-bold fs-16'>{session.showTime}</span> ~ <span>{new Date(`01/01/1970 ${session.showTime}`).addHours(film.duration / 60).toTimeString().replace(/.*(\d{2}:\d{2}):\d{2}.*/, "$1")}</span>
                                                                        </div>
                                                                    </>
                                                                })}
                                                            </div>
                                                        </>
                                                    })
                                                }
                                            </div>
                                        </div>
                                    })
                                }
                            </div>
                            :
                            <NoData />
                    }
                </div>
            </div>
            <Footer />
            {
                isOpenMap && <ModalMapCine setOpenFunc={setIsOpenMap} Cine={state.cineplex} />
            }
        </>
    )
}