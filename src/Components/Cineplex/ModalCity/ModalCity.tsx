import { eCity } from 'Components/Interface/eCineplexDetail';
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';
import APIs from 'service/APIs';



import './ModalCity.scss'

interface eProps {
    setOpenFunc: (isOpen: boolean) => void,
    currentCity: eCity | undefined,
    setCurrentCity: (city: eCity) => void
}

export default function ModalCity(props: eProps) {
    const [cities, setCities] = useState<Array<eCity>>([]);
    const [searchCity, setSearchCity] = useState("");

    useEffect(() => {
        fetch(APIs.GET_CITY)
            .then(res => res.json())
            .then(city => {
                setCities(city);
            })
    }, [])

    const handlerSearchCity = (e: React.KeyboardEvent<HTMLInputElement>) => {
        setSearchCity(e.currentTarget.value.toLowerCase());
    }

    const handlerSelectCity = (city: eCity) => {
        props.setCurrentCity(city);
        props.setOpenFunc(false);
    }

    return (
        <div className='modal-city d-flex align-items-center justify-content-center' onClick={() => props.setOpenFunc(false)}>
            <div className='main border-rad-15 position-relative d-flex flex-col' onClick={(event) => event.stopPropagation()}>
                <div className='header d-flex justify-content-end'>
                    <i onClick={() => props.setOpenFunc(false)} className="fa-solid fa-x"></i>
                </div>
                <div className='content px-10'>
                    <div className='d-flex justify-content-between mb-20'>
                        <h3>Chọn địa điểm</h3>
                        <div className='search'>
                            <input onKeyUp={handlerSearchCity} type="text" placeholder='Tìm địa điểm ...' />
                        </div>
                    </div>
                    <div className="city-list">
                        {
                            cities
                                .filter((city: eCity) => {
                                    return city.name.toLowerCase().includes(searchCity);
                                })
                                .map((city: eCity, cityIndex) => {
                                    return <div onClick={() => handlerSelectCity(city)} key={cityIndex} className={`city ${city.name === props.currentCity?.name ? 'active' : ''}`}>
                                        {city.name}
                                    </div>
                                })
                        }
                    </div>
                </div>
                <div className="footer py-10 border-box d-flex justify-content-end">
                    <button onClick={() => props.setOpenFunc(false)}>Đóng</button>
                </div>
            </div>
        </div>
    )
}