import eCinema from 'Components/Interface/eCinema';
import { eCity } from 'Components/Interface/eCineplexDetail';
import Footer from 'Components/UI/Footer/Footer'
import Header from 'Components/UI/Header/Header'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import APIs from 'service/APIs';

import './Cineplex.scss';
import ModalCity from './ModalCity/ModalCity';
import ModalMapCine from '../UI/ModalMapCine/ModalMapCine';


const cineplexBrand = [
    {
        name: "Tất cả",
        logo: "https://static.mservice.io/next-js/_next/static/public/cinema/dexuat-icon.svg",
        statusActive: true,
    },
    {
        name: "Galaxy Cinema",
        logo: "https://static.mservice.io/cinema/momo-upload-api-211123095138-637732578984425272.png",
        statusActive: true,
    },
    {
        name: "CGV",
        logo: "https://static.mservice.io/placebrand/s/momo-upload-api-190709165424-636982880641515855.jpg",
        statusActive: false,

    },
    {
        name: "Lotte Cinema",
        logo: "https://static.mservice.io/blogscontents/momo-upload-api-210604170617-637584231772974269.png",
        statusActive: false,

    },

    {
        name: "BHD Star",
        logo: "https://static.mservice.io/blogscontents/momo-upload-api-210604170453-637584230934981809.png",
        statusActive: false,

    },
    {
        name: "Beta Cinemas",
        logo: "https://static.mservice.io/cinema/momo-upload-api-210813104719-637644484394328824.png",
        statusActive: false,

    },
    {
        name: "Cinemax",
        logo: "https://static.mservice.io/cinema/momo-upload-api-221108100132-638034984925107129.png",
        statusActive: false,

    },
    {
        name: "DCINE",
        logo: "https://img.mservice.io/momo_app_v2/new_version/img/THAO.MAI/DcineLogo.png",
        statusActive: false,

    }
];

export default function Cineplex() {
    const navigate = useNavigate();

    const [isOpenModal, setIsOpenModal] = useState(false);
    const [isOpenMap, setIsOpenMap] = useState(false);
    const [currenrtMapCine, setCurrenrtMapCine] = useState<eCinema>();
    const [searchText, setSearchText] = useState("");
    const [cineplexs, setCineplexs] = useState<Array<eCinema>>([]);

    const [currentCity, setCurrentCity] = useState<eCity>({
        "id": "599535ea-1ea2-4393-9b5a-3ba3a807f363",
        "name": "TP Hồ Chí Minh",
        "slug": "tp-ho-chi-minh",
        "description": "<!--| -->",
        "district": []
    });

    useEffect(() => {
        fetch(APIs.GET_ALL_CINEMAS)
            .then(res => res.json())
            .then(data => {
                setCineplexs(data)
            });
    }, [])

    const handleSearch = (e: React.FormEvent<HTMLInputElement>): void => {
        setSearchText(e.currentTarget.value);
    }

    const handleClickCineplex = (cineplex: eCinema) => {
        navigate('/cinema/galaxy/' + cineplex.slug, {
            state: {
                cineplex: cineplex
            }
        })
    }

    const handleShowMapEmbeb = (event: any, Cine: eCinema) => {
        if (event.target === event.currentTarget) {
            setIsOpenMap(true);
            setCurrenrtMapCine(Cine)
            event.stopPropagation();
        }
    }

    return (
        <>
            <Header />
            <div className="cineplex d-flex flex-col align-items-center">
                <h1 className='mt-40 mb-20'>Tìm rạp chiếu phim MoMo</h1>
                <div className='main px-20 py-30 mb-40 border-box'>
                    <div className="location d-flex p-10 align-items-center">
                        <h3 className='fs-16 mr-15'>Vị trí</h3>
                        <div onClick={() => setIsOpenModal(true)} className="city d-inline-block border-box py-5 px-10">
                            <span className='d-flex align-items-center fs-14 fw-bold'>
                                <i className="mr-10 fa-solid fa-location-dot"></i>
                                <p>{currentCity?.name}</p>
                                <i className="ml-10 fa-solid fa-caret-down"></i>
                            </span>
                        </div>
                    </div>
                    <div className='brands-list d-flex justify-content-start flex-wrap mb-5'>
                        {
                            cineplexBrand.map((brand, i) => {
                                return <>
                                    <div key={i} className="brand-item border-box m-10 d-flex flex-col align-items-center">
                                        <img src={brand.logo} alt="logo cinema" className={`w-100 border-box mb-10 ${brand.statusActive && 'active'}`} />
                                        <p className={`text-center fs-12 w-100 ${brand.statusActive && 'active'}`}>{brand.name}</p>
                                    </div>
                                </>
                            })
                        }
                    </div>

                    <div className="border-bottom"></div>

                    <div className='search my-10'>
                        <input className='w-100 p-15 border-box' onKeyUp={handleSearch} placeholder='Tìm phim theo rạp...' type="text" />
                    </div>

                    <div className="border-bottom"></div>

                    <div className="cineplex-list">
                        {
                            cineplexs
                                .filter((cineplex) => cineplex.name.toLowerCase().includes(searchText.toLowerCase()))
                                .map((cineplex, i) => {
                                    return <div key={i} onClick={() => handleClickCineplex(cineplex)} className="cineplex-item border-box d-flex">
                                        <img src="https://static.mservice.io/cinema/momo-upload-api-211123095138-637732578984425272.png" alt="" />
                                        <div className='cineplex-item__info ml-20'>
                                            <h4>{cineplex.name} <span onClick={(event) => handleShowMapEmbeb(event, cineplex)}>[Bản đồ]</span></h4>
                                            <p className='fs-14'>{cineplex.address}</p>
                                        </div>
                                    </div>
                                    // return <div key={i} onClick={() => handleClickCineplex(cineplex)} className="cineplex-item border-box d-flex">
                                    //     <img src="https://static.mservice.io/cinema/momo-upload-api-211123095138-637732578984425272.png" alt="" />
                                    //     <div className='cineplex-item__info ml-20'>
                                    //         <h4>{cineplex.name} <span><a href="#">[Bản đồ]</a></span></h4>
                                    //         <p className='fs-14'>{cineplex.address}</p>
                                    //     </div>
                                    // </div>
                                })
                        }
                    </div>
                </div>
            </div>
            <Footer />

            {
                isOpenModal && <ModalCity currentCity={currentCity} setCurrentCity={setCurrentCity} setOpenFunc={setIsOpenModal} />
            }
            {
                isOpenMap && <ModalMapCine setOpenFunc={setIsOpenMap} Cine={currenrtMapCine} />
            }
        </>
    )
}

