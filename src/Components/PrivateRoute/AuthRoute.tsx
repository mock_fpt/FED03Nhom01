import Home from 'Components/Home/Home';
import React, { useEffect, useState } from 'react'
import useCookies from 'react-cookie/cjs/useCookies';
import { Routes } from 'react-router-dom';


interface eProps {
    requiredAuth?: true,
    requiredGuest?: true,
    children: JSX.Element
}

export default function PrivateRoute(props: eProps) {
    const [accept, setAccept] = useState(false);
    const [cookies, setCookies] = useCookies(['userInfo']);

    useEffect(() => {
        isCheckCondition();
    })

    const isCheckCondition = () => {
        if (props.requiredAuth) {
            if (cookies.userInfo) {
                setAccept(true);
            }
        } else if (props.requiredGuest) {
            if (!cookies.userInfo) {
                setAccept(true);
            }
        }
    }

    return (
        <div>
            <Routes>
                {
                    accept && props.children
                }
            </Routes>
        </div>
    )
}
