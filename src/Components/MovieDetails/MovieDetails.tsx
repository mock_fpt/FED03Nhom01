/* eslint-disable no-extend-native */
import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'

import eListAllMovie, { eMovieInfo } from "Components/Interface/eMovieInfo";

import Footer from 'Components/UI/Footer/Footer';
import Header from 'Components/UI/Header/Header';
import './MovieDetails.scss'
import APIs from 'service/APIs';
import NoData from 'Components/UI/PagesError/NoData/NoData';
import { eCity, eTicket } from 'Components/Interface/eCineplexDetail';
import ModalCity from 'Components/Cineplex/ModalCity/ModalCity';
import ModalTrailer from 'Components/UI/ModalTrailer/ModalTrailer';
import ModalMapCine from 'Components/UI/ModalMapCine/ModalMapCine';
import eCinema from 'Components/Interface/eCinema';
//====INTERFACE: START
interface eDate {
    date: string,
    day: string,
    fullDay: string,
}
interface eApiListMovie {
    statusApi: number,
    data: eListAllMovie,
}
interface eSchedule {
    dayOfWeekLabel: string,
    showDate: string,
    bundles: [
        {
            caption: string,
            version: string,
            code: string,
            sessions: [
                {
                    showDate: string
                    showTime: string,
                    cinemaId: string
                }
            ]
        }
    ]
}
interface eListShowTimes {
    address: string,
    code: string,
    name: string,
    dates: eSchedule[],
}
//====INTERFACE:END

const cinemaBrands = [
    {
        name: "Tất cả",
        urlImg: "https://static.mservice.io/next-js/_next/static/public/cinema/dexuat-icon.svg",
        statusActive: true,
    },
    {
        name: "Galaxy Cinema",
        urlImg: "https://static.mservice.io/cinema/momo-upload-api-211123095138-637732578984425272.png",
        statusActive: true,
    },
    {
        name: "CGV",
        urlImg: "https://static.mservice.io/placebrand/s/momo-upload-api-190709165424-636982880641515855.jpg",
        statusActive: false,
    },
    {
        name: "Lotte Cinema",
        urlImg: "https://static.mservice.io/blogscontents/momo-upload-api-210604170617-637584231772974269.png",
        statusActive: false,
    },
    {
        name: "BHD Star",
        urlImg: "https://static.mservice.io/blogscontents/momo-upload-api-210604170453-637584230934981809.png",
        statusActive: false,
    },
    {
        name: "Beta Cinemas",
        urlImg: "https://static.mservice.io/cinema/momo-upload-api-210813104719-637644484394328824.png",
        statusActive: false,
    },
    {
        name: "Cinemax",
        urlImg: "https://static.mservice.io/cinema/momo-upload-api-221108100132-638034984925107129.png",
        statusActive: false,
    },
    {
        name: "DCINE",
        urlImg: "https://img.mservice.io/momo_app_v2/new_version/img/THAO.MAI/DcineLogo.png",
        statusActive: false,
    }
]
const DaysOfWeek = ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Hôm nay'];

function MovieDetails(props: any) {
    const navigate = useNavigate()
    const [tickets, setTickets] = useState<Array<eTicket>>();

    const [isOpenModal, setIsOpenModal] = useState(false);
    const [isOpenTrailer, setIsOpenTrailer] = useState(false);
    const [isOpenMapCine, setIsOpenMapCine] = useState(false);
    const [currenrtMapCine, setCurrenrtMapCine] = useState<eCinema>();
    const [currentCity, setCurrentCity] = useState<eCity>({
        "id": "599535ea-1ea2-4393-9b5a-3ba3a807f363",
        "name": "TP Hồ Chí Minh",
        "slug": "tp-ho-chi-minh",
        "description": "<!--| -->",
        "district": []
    });

    const { movieId } = useParams();
    const [movieInfo, setMovieInfo] = useState<eMovieInfo>();
    const [listAllMovies, setListAllMovies] = useState<eApiListMovie>({
        statusApi: 0,
        data: {
            movieShowing: [],
            movieCommingSoon: []
        },
    });

    const [lstDatesFormat, createLstDatesFormat] = useState<eDate[]>()

    useEffect(() => {
        const addDays = (stepSize: number) => {
            let currentDay = new Date();
            currentDay.setDate(currentDay.getDate() + stepSize);
            return currentDay;
        };

        let arrDates = Array.from({ length: 10 }, (_, i) => {
            let current = (new Date())
            let tempDate = addDays(i).getDate() + '';
            let tempDay = (current.toLocaleDateString() === addDays(i).toLocaleDateString())
                ? DaysOfWeek[7]
                : DaysOfWeek[addDays(i).getDay()]
            let tempFullDay = addDays(i).customFormat('#DD#/#MM#/#YYYY#');
            return {
                date: tempDate,
                day: tempDay,
                fullDay: tempFullDay,
            }
        });
        createLstDatesFormat(arrDates);
    }, []);

    useEffect(() => {
        FETCH_ALL_MOVIE();

        fetch(APIs.GET_BOOKING_DETAILS)
            .then(res => res.json())
            .then(data => {
                setTickets(data.ticket);
            })
    }, [])


    useEffect(() => {
        if (listAllMovies.statusApi === 200) {
            setMovieInfo(handleMovieDetail(listAllMovies.data));
        }
    }, [listAllMovies]);

    const [booking, setBooking] = useState<Array<eListShowTimes>>([]);
    const [keyCine, setKeyCine] = useState<string>('')

    useEffect(() => {
        if (movieId) {
            fetch(APIs.GET_MOVIE_BY_ID(movieId))
                .then(res => res.json())
                .then(data => {
                    setBooking(data);
                    setKeyCine(data[0].code)
                })
        }
    }, [movieInfo])

    const curruntDay = (new Date()).customFormat('#DD#/#MM#/#YYYY#')
    const [keyDate, setKeyDate] = useState(curruntDay);
    const [resultData, setResultData] = useState<{ lstCine: eListShowTimes[], cine: eSchedule[] }>()

    useEffect(() => {
        let result = locDataTest()
        setResultData({
            lstCine: result.dsRaptheoKeyDate,
            cine: result.dsLichchieutheoKeyDateandKeyCine
        })
    }, [booking, keyDate, keyCine])

    const filterCinesByDate = (keyDate: string) => {
        return booking.filter(ele => {
            for (let i = 0; i < ele.dates.length; i++) {
                if (ele.dates[i].showDate === keyDate) return ele;
            }
        });
    }

    const handleMovieDetail = (data: { movieShowing: any, movieCommingSoon: any }) => {
        return [
            ...data.movieShowing.filter((e: any) => e.id.includes(movieId)),
            ...data.movieCommingSoon.filter((e: any) => e.id.includes(movieId)),
        ][0];
    }

    const FETCH_ALL_MOVIE = async () => {
        let res = await fetch(APIs.GET_ALL_MOVIES);
        let rawData = await res.json();
        setListAllMovies({
            statusApi: res.status,
            data: rawData,
        });
    };

    const locDataTest = () => {
        const filterScheduleByKeyDateandKeyCine = (keyDate: string, keyCine: string, data: eListShowTimes[]) => {
            let result = [];
            for (let i = 0; i < data.length; i++) {
                if (data[i].code === keyCine) {
                    for (let j = 0; j < data[i].dates.length; j++) {
                        if (data[i].dates[j].showDate === keyDate) result.push(data[i].dates[j])
                    }
                }
            }
            return result
        }
        let dsRaptheoKeyDate = filterCinesByDate(keyDate);
        let dsLichchieutheoKeyDateandKeyCine = filterScheduleByKeyDateandKeyCine(keyDate, keyCine, dsRaptheoKeyDate)
        return {
            dsRaptheoKeyDate: dsRaptheoKeyDate,
            dsLichchieutheoKeyDateandKeyCine: dsLichchieutheoKeyDateandKeyCine
        }
    }

    const handlerSelectScheduleFilm = (film: eMovieInfo, session: any, cineplex: any) => {
        navigate("/booking/seats", {
            state: {
                tickets: tickets,
                film: film,
                session: session,
                cineplex: cineplex,
            }
        });
    }

    const convertDuration = (duration: string) => {
        let minutes = Number(duration) % 60;
        let hour = Math.floor(Number(duration) / 60);
        return `${hour} giờ ${minutes ? `${minutes} phút` : ''}`
    }

    const handleScroll = (id: string) => {
        const element = document.getElementById(id);
        window.scrollTo({
            behavior: element ? 'smooth' : 'auto',
            top: element ? element.offsetTop - 20 : 0
        })
    }

    const handleShowMapEmbeb = (codeCine: string) => {
        if (resultData) {
            let a: any = resultData.lstCine.filter(e => {
                if (e.code === codeCine) return e
            })
            setIsOpenMapCine(true);
            setCurrenrtMapCine(a[0])
        }
    }

    return (<>
        <Header />
        {
            movieInfo && <>
                <div className="movie-details">
                    <div className='header-details position-relative'>
                        <div className="banner position-relative" style={{ backgroundImage: `url('${movieInfo.imageLandscape}')` }}>
                            <i className="fa-regular fa-circle-play position-absolute" onClick={() => setIsOpenTrailer(true)}></i>
                        </div>
                        <div className="title border-box d-flex justify-content-center">
                            <div className='border-box'>
                                <div className="poster position-absolute">
                                    <img className='w-100' src={`${movieInfo.imagePortrait}`} alt="poster movie" />
                                </div>
                                <div className="info pt-10 py-30 pl-20 border-box d-inline-block">
                                    <div className="row">
                                        <span className={'fs-12 fw-bold ' + (movieInfo.age === "0" ? "age-p" : movieInfo.age === "13" ? "age-13" : movieInfo.age === "16" ? "age-16" : movieInfo.age === "18" ? "age-18" : "")}>{movieInfo.age !== "0" ? `${movieInfo.age}+` : "P"}</span>
                                        <h1 className='fs-30 mt-5 mb-10'>{movieInfo.name}</h1>
                                        <p className='fs-16 mb-15'>{movieInfo.subName}</p>
                                    </div>
                                    <div className="row d-flex align-items-center mb-10">
                                        <p className='mr-15'><i className="fa-regular fa-clock mr-5"></i>{convertDuration(movieInfo.duration)}</p>
                                        <p className='mr-15'><i className="fa-solid fa-calendar-days mr-5"></i> {(new Date(movieInfo.startdate)).customFormat('#DD#/#MM#/#YYYY#')}</p>
                                        <div className='d-flex align-items-center fs-18'>
                                            <i style={{ color: '#f5c518' }} className="fa-solid fa-star mr-5"></i>
                                            <p className='fw-bold'> {movieInfo.point.toFixed(2)} <em className='fs-12' style={{ fontWeight: "normal" }}>/10</em></p>
                                        </div>
                                    </div>
                                    <div>
                                        <h5 className='fs-18 mb-5'>Nội dung</h5>
                                        <p className='des fs-16'>{convertToPlain(movieInfo.description)}</p>
                                    </div>
                                    <button className="border-0 fs-18" onClick={() => handleScroll('lich-chieu')}>Đặt vé</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id='lich-chieu' className="main-detail d-flex flex-col align-items-center py-40">
                        <div className="title d-flex align-items-center justify-content-between py-10">
                            <h1 className='fs-20 my-15'><span className='fs-18'>Lịch chiếu </span>{movieInfo.name}</h1>
                            <div className="location d-flex align-items-center">
                                <div className="border-box mr-10">
                                    <span onClick={() => setIsOpenModal(true)} className='d-flex align-items-center fs-14 fw-bold'>
                                        <i className="mr-10 fa-solid fa-location-dot"></i>
                                        {currentCity.name}
                                        <i className="ml-10 fa-solid fa-caret-down"></i>
                                    </span>
                                </div>
                                <div className="border-box ">
                                    <span className='d-flex align-items-center fs-14 fw-bold'>
                                        <i className="mr-10 fas fa-location"></i>
                                        Gần bạn
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className='main p-20 border-box mb-10'>
                            <div className='brands-list d-flex justify-content-start flex-wrap mb-5'>
                                {
                                    cinemaBrands.map((brand, i) => {
                                        return <>
                                            <div key={i} className="brand-item border-box m-10 d-flex flex-col align-items-center">
                                                <img src={brand.urlImg} alt="logo cinema" className={`w-100 border-box mb-10 ${brand.statusActive && 'active'}`} />
                                                <p className={`text-center fs-12 w-100 ${brand.statusActive && 'active'}`}>{brand.name}</p>
                                            </div>
                                        </>
                                    })
                                }
                            </div>

                            <div className="divider my-5"></div>

                            <div className='list-dates my-10'>
                                {
                                    lstDatesFormat?.map(e => <>
                                        <div className={`date-item border-box m-5 text-center ${e.fullDay === keyDate ? 'active' : ''}`} onClick={() => setKeyDate(e.fullDay)}>
                                            <div className='py-5 fs-16 fw-bold fs-18'>{e.date}</div>
                                            <div className='py-5 fs-12'>{e.day}</div>
                                        </div>
                                    </>)
                                }
                            </div>

                            <div className="divider mt-5 mb-10"></div>
                            <div className="cinemas-list border-box pr-10">
                                {
                                    resultData?.lstCine && resultData.lstCine.length > 0
                                        ? resultData.lstCine.map(cine => {
                                            return <>
                                                <div className="cine-item">
                                                    <div className="info-cine py-10 d-flex align-items-center"
                                                        onClick={() => setKeyCine(cine.code)}>
                                                        <img src='https://static.mservice.io/cinema/momo-upload-api-211123095138-637732578984425272.png' alt="logo cinema" className='mr-15' />
                                                        <div className='name'>
                                                            <h4 className='fs-18'>{cine.name}</h4>
                                                            <p className='d-inline-block mr-10 fs-14'>{cine.address}</p>
                                                            <span className='d-inline-block fs-14 text-decoration-none' onClick={() => handleShowMapEmbeb(cine.code)}>[Bản đồ]</span>
                                                            {/* <a className='d-inline-block fs-14 text-decoration-none' href='/'>[Bản đồ]</a> */}
                                                        </div>
                                                        <div className='icon px-10 fs-18'>
                                                            <i className="fa-solid fa-angle-down"></i>
                                                        </div>
                                                    </div>
                                                    <div className="list-showtime">
                                                        {
                                                            cine.code === keyCine && resultData.cine.map(ele => {
                                                                return <>
                                                                    {
                                                                        ele.bundles.map(item => {
                                                                            return <>
                                                                                <div className="">
                                                                                    <h4 className='fs-16 mb-5'>{[item.version.toUpperCase()]}</h4>
                                                                                    <div className='pb-15'>
                                                                                        {
                                                                                            item.sessions.map(session => {
                                                                                                return <>
                                                                                                    <div className='showtime-item text-center py-5 border-box' onClick={() => handlerSelectScheduleFilm(movieInfo, session, cine)}>
                                                                                                        <span className='fs-14 fw-bolder'>{session.showTime}</span> ~ <span className='fs-12'>{new Date(`01/01/1970 ${session.showTime}`).addHours(parseInt(movieInfo.duration) / 60).toTimeString().replace(/.*(\d{2}:\d{2}):\d{2}.*/, "$1")}</span>
                                                                                                    </div>
                                                                                                </>
                                                                                            })
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                            </>
                                                                        })
                                                                    }
                                                                </>
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                                <div className="divider"></div>
                                            </>
                                        })
                                        : <NoData />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </>
        }
        <Footer />

        {isOpenModal &&
            <ModalCity currentCity={currentCity} setCurrentCity={setCurrentCity} setOpenFunc={setIsOpenModal} />}
        {isOpenTrailer && movieInfo &&
            <ModalTrailer infoMovie={movieInfo} setOpenFunc={setIsOpenTrailer} />}
        {isOpenMapCine && <ModalMapCine setOpenFunc={setIsOpenMapCine} Cine={currenrtMapCine} />}
    </>
    )
}

function convertToPlain(html: any) {
    // Create a new div element
    let tempDivElement = document.createElement('div');
    // Set the HTML content with the given value
    tempDivElement.innerHTML = html;
    // Retrieve the text property of the element
    return tempDivElement.textContent || tempDivElement.innerText || "";
}
export default (MovieDetails)