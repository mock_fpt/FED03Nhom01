import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';

import Header from 'Components/UI/Header/Header'
import Footer from 'Components/UI/Footer/Footer'

import './Cinema.scss';
import eCinema from 'Components/Interface/eCinema';
import APIs from 'service/APIs';

//==== Declare interface use Cinema component
interface eCineBrands {
    name: string,
    urlImg: string,
    statusActive: boolean,
}
interface eApiAllCinema {
    statusApi: number,
    data: eCinema[],
}

const cineBrands: Array<eCineBrands> = [
    {
        name: "Tất cả",
        urlImg: "https://static.mservice.io/next-js/_next/static/public/cinema/dexuat-icon.svg",
        statusActive: true,
    },
    {
        name: "Galaxy Cinema",
        urlImg: "https://static.mservice.io/cinema/momo-upload-api-211123095138-637732578984425272.png",
        statusActive: true,
    },
    {
        name: "CGV",
        urlImg: "https://static.mservice.io/placebrand/s/momo-upload-api-190709165424-636982880641515855.jpg",
        statusActive: false,
    },
    {
        name: "Lotte Cinema",
        urlImg: "https://static.mservice.io/blogscontents/momo-upload-api-210604170617-637584231772974269.png",
        statusActive: false,
    },
    {
        name: "BHD Star",
        urlImg: "https://static.mservice.io/blogscontents/momo-upload-api-210604170453-637584230934981809.png",
        statusActive: false,
    },
    {
        name: "Beta Cinemas",
        urlImg: "https://static.mservice.io/cinema/momo-upload-api-210813104719-637644484394328824.png",
        statusActive: false,
    },
    {
        name: "Cinemax",
        urlImg: "https://static.mservice.io/cinema/momo-upload-api-221108100132-638034984925107129.png",
        statusActive: false,
    },
    {
        name: "DCINE",
        urlImg: "https://img.mservice.io/momo_app_v2/new_version/img/THAO.MAI/DcineLogo.png",
        statusActive: false,
    }
]


function Cinema() {

    const [listAllCine, setlistAllCine] = useState<eApiAllCinema>({
        statusApi: 0,
        data: [],
    })
    const [searchText, setSearchText] = useState("");

    const navigate = useNavigate();
    const handleClickCinema = (cinema : eCinema) => {
        navigate('/cinema/galaxy/' + cinema.slug, {
            state: {
                cinema: cinema
            }
        })
    }

    useEffect(() => {
        const FETCH_ALL_CINEMAS = async () => {
            console.log("API FETCH ALL CINEMA: START");
            let res = await fetch(APIs.GET_ALL_CINEMAS);
            let rawData = await res.json();
            setlistAllCine({
                statusApi: res.status,
                data: rawData
            });
            console.log("API FETCH ALL CINEMA: END");
        };
        FETCH_ALL_CINEMAS();
    }, [])


    const handleSearch = (e: React.FormEvent<HTMLInputElement>): void => {
        setSearchText(e.currentTarget.value,);
    }

    return (<>
        <Header />
        <div className="cinema-page d-flex flex-col align-items-center my-30">
            <h1 className='mb-20'>Tìm rạp chiếu phim</h1>
            <div className='main p-20 border-box mb-10'>
                <div className="location d-flex align-items-center p-10">
                    <h3 className='fs-16 mr-15'>Vị trí</h3>
                    <div className="city border-box mr-10">
                        <span className='d-flex align-items-center fs-14 fw-bold'>
                            <i className="mr-10 fa-solid fa-location-dot"></i>
                            TP.HCM
                            <i className="ml-10 fa-solid fa-caret-down"></i>
                        </span>
                    </div>
                    <div className="city border-box mr-10">
                        <span className='d-flex align-items-center fs-14 fw-bold'>
                            <i className="mr-10 fas fa-location"></i>
                            Gần bạn
                        </span>
                    </div>
                </div>

                <div className='brands-list d-flex justify-content-start flex-wrap mb-15'>
                    {
                        cineBrands.map((brand, i) => {
                            return <>
                                <div key={i} className="brand-item border-box m-10 d-flex flex-col align-items-center">
                                    <img src={brand.urlImg} alt="logo cinema" className={`w-100 border-box mb-10 ${brand.statusActive && 'active'}`} />
                                    <p className={`text-center fs-12 w-100 ${brand.statusActive && 'active'}`}>{brand.name}</p>
                                </div>
                            </>
                        })
                    }
                </div>

                <div className="divider"></div>

                <div className='search mt-20 mb-10'>
                    <input onKeyUp={handleSearch} placeholder='Tìm phim theo rạp rạp...' type="text" className='w-100 p-15 border-box' />
                </div>

                <div className="divider"></div>
                <div className="cinema-list w-100">
                    {
                        listAllCine.statusApi === 200 && listAllCine.data.length > 0
                            ? listAllCine.data
                                .filter((cine: eCinema) => cine.name.toLowerCase().includes(searchText.toLowerCase()))
                                .map((cine: eCinema) => {
                                    return <>

                                        <div key={cine.code} className="cinema-item border-box d-flex">
                                            <Link to={'/cinema/' + cine.slug}>
                                                <img src='https://static.mservice.io/cinema/momo-upload-api-211123095138-637732578984425272.png' alt="logo cinema" />
                                            </Link>
                                            <div className='ml-20 flex-grow-1'>
                                                <Link to={'/cinema/galaxy/' + cine.slug} className='mr-15'>
                                                    <h4 className='d-inline-block fs-16'>{cine.name}</h4>
                                                </Link>
                                                <span>
                                                    <Link to={`/cinema/${cine.slug}/map`} className='fs-14'>[Bản đồ]</Link>
                                                </span>
                                                <p>{cine.address}</p>
                                            </div>
                                        </div>
                                    </>
                                })
                            : ''
                    }
                </div>
            </div>
        </div>
        <Footer />
    </>
    )
}

const mapStateToProps = (state: any) => {
    return {

    }
}
const mapDispatchToProps = (dispatch: any) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cinema);
