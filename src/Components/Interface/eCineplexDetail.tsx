export interface eFilm {  
    name: string,
    duration: number,
    subname: string,
    age: string,
    imagePortrait: string,
    dates: [
        {
            showDate: string,
            dayOfWeekLabel: string,
            bundles: eBundles[]
        }
    ]
}

export interface eBundles {
    version: string,
    sessions: [
        {
            showTime: string
        }
    ]
}

export interface eTicket {
    "name": string,
    "description": string,
    "category": string,
    "price": number,
    "displayPrice": number,
    "defaultQuantity": number,
    "ticketTypeCode": string,
    "areaCategoryCode": string,
    "onlyMember": boolean,
}

export interface eCity {
    "id": string,
    "name": string,
    "slug": string,
    "description": string,
    "district": []
}