export default interface eCinema {
    'id': string,
    'name': string,
    'address': string,
    'slug': string,
    'code': string,
    'phone': string,
    'mapEmbeb': string,
    'cityId': string,
    'imagePortrait'?: null,
    'imageLandscape': string,
    'longitude': string,
    'latitude': string,
    'imageUrls': Array<string>,
    'ticket': [
        {
            'name'?: string,
            'url': string
        }
    ],
}