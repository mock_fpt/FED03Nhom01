export interface ePosition {
    "areaNumber": number,
    "columnIndex": number,
    "rowIndex": number
}

export interface eSeats {
    "position": ePosition,
    "priority": number,
    "id": string,
    "status": number,
    "seatStyle": number,
    "seatsInGroup": Array<Object>,
    "originalStatus": number
}
  
export interface eRows {
    "physicalName": string,
    "seats": Array<eSeats>
}
  
export interface eAreas {
    "number": number,
    "areaCategoryCode": string,
    "description": string,
    "descriptionAlt": string,
    "numberOfSeats": number,
    "isAllocatedSeating": boolean,
    "hasSofaSeatingEnabled": boolean,
    "left": number,
    "top": number,
    "height": number,
    "width": number,
    "rows": Array<eRows>,
    "rowCount": number,
    "columnCount": number
}