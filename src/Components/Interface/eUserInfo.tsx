export default interface eUserInfo {
    "Email"?: string,
    "Name"?: string,
    "Role"?: number,
    "Password"?: string
}