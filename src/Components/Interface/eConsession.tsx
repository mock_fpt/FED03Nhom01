export default interface eConsession {
    id: string,
    description: string,
    extendedDescription: string,
    imageUrl: string,
    price: number,
    displayPrice: number,
    defaultQuantity: number,
}