export interface eMovieInfo {
    'id': string,
    'slug': string,
    'name': string,
    'subName'?: string,
    'description': string,
    'shortDescription'?: string,
    'trailer': string,
    'age': string,
    'duration': string,
    'views': number,
    'startdate': string,
    'enddate': string,
    'imageLandscape': string,
    'imagePortrait': string,
    'point': number,
    'totalVotes': number,
}

export default interface eListAllMovie {
    movieShowing: eMovieInfo[],
    movieCommingSoon: eMovieInfo[],
}