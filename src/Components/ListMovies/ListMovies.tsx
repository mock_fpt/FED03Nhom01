import React, { useEffect, useState } from 'react'
import Footer from 'Components/UI/Footer/Footer'
import Header from 'Components/UI/Header/Header'
import CardMovie from 'Components/UI/CardMovie/CardMovie'

import './ListMovies.scss'
import { connect } from 'react-redux'
import APIs from 'service/APIs'
import eListAllMovie, { eMovieInfo } from "Components/Interface/eMovieInfo";
import NoData from 'Components/UI/PagesError/NoData/NoData'
import { useLocation } from 'react-router-dom'

const NavTypeMovieItems = [
    {
        name: "Lịch chiếu phim",
        url: "now-showing",
        key: 'movieShowing',
    },
    {
        name: "Phim sắp chiếu",
        url: "coming-soon",
        key: 'movieCommingSoon',
    },
];

interface listAllMovie extends eListAllMovie {
    [key: string]: eMovieInfo[]
}

function ListMovies() {
    const [listAllMovie, setListAllMovie] = useState<listAllMovie>();
    const { hash } = useLocation();

    const handleOnClick = (hash: string) => {
        const id = hash.replace('#', '');
        const element = document.getElementById(id);
        window.scrollTo({
            behavior: element ? 'smooth' : 'auto',
            top: element ? element.offsetTop - 20 : 0
        })
    };

    useEffect(() => {
        handleOnClick(hash)
    }, [hash, listAllMovie])

    useEffect(() => {
        fetch(APIs.GET_ALL_MOVIES)
            .then(res => res.json())
            .then(data => {
                setListAllMovie(data);
            })
    }, []);
    return (<>
        <Header />
        <div className='list-movie-pages d-flex flex-col align-items-center' >
            {
                NavTypeMovieItems.map(item => {
                    return <>
                        <div key={item.key} id={item.url} className='px-20 pt-20 pb-40 border-box' style={{ background: (item.url === 'coming-soon') ? '#fafafa' : '' }}>
                            <h1 className='fs-30 my-15'>{item.name}</h1>
                            <div className="list-movie mainSize d-flex flex-wrap mb-10">
                                {
                                    listAllMovie && listAllMovie[item.key]
                                        ? listAllMovie[item.key].map((movie) => {
                                            return <CardMovie infoMovie={movie} sizeItem='w-20' />
                                        })
                                        : <NoData />
                                }
                            </div>
                        </div>
                    </>
                })
            }
        </div>
        <Footer />
    </>
    )
}

const mapStateToProps = (state: { rdcAllMovies: {}[] }) => {
    return {
    }
}
const mapDispatchToProp = (dispatch: any) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProp)(ListMovies)