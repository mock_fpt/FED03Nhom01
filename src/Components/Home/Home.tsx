import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom';
import Header from 'Components/UI/Header/Header';
import Footer from 'Components/UI/Footer/Footer';

import './Home.scss'
import Slider from './Slider/Slider';
import Introduce from './Introduce/Introduce';
import CardMovie from 'Components/UI/CardMovie/CardMovie';
import MoreInfo from './MoreInfo/MoreInfo';
import { connect } from 'react-redux';
import Action from 'Components/Redux/Action/Action';

const NavTypeMovieItems = [
  {
    name: "Đang chiếu",
    url: "now-showing",
    key: 'movieShowing',
  },
  {
    name: "Sắp chiếu",
    url: "coming-soon",
    key: 'movieCommingSoon',
  },
];

function Home(props: any) {
  const [typeMovie, setTypeMovie] = useState('now-showing');
  const [listMoviesbyType, setlistMoviesbyType] = useState([]);

  useEffect(() => {
    props.fetchLstAllMovie();
  }, []);

  useEffect(() => {
    switch (typeMovie) {
      case 'coming-soon':
        setlistMoviesbyType(props.allMovies.lstComingSoon);
        break;
      default:
        setlistMoviesbyType(props.allMovies.lstNowShowing);
        break;
    }
  }, [typeMovie, props.allMovies]);
  return (
    <div>
      <Header />
      {
        props.allMovies.lstNowShowing
        && props.allMovies.lstNowShowing.length > 0
        && <>
          <Slider films={props.allMovies.lstNowShowing.slice(0, 4)} />
        </>
      }
      <Introduce />
      <div className="main-home mt-20 mb-40">
        <div className='text-center btn-choose'>
          {
            NavTypeMovieItems.map((e) => {
              return <>
                <button className={`btnType border-0 fs-25 fw-bold mx-20 py-10 ${typeMovie === e.url ? 'active' : ''}`} onClick={() => setTypeMovie(e.url)}>
                  <span>{e.name}</span>
                </button>
              </>
            })
          }
        </div>
        <div className='list-movie mb-20'>
          <div className='d-flex flex-col justify-content-center align-items-center'>
            {
              props.allMovies && typeMovie && Object.keys(props.allMovies).length > 0 && <>
                <div className="mainSize d-flex flex-wrap">
                  {
                    listMoviesbyType.slice(0, 4).map((e: any) => {
                      return <CardMovie infoMovie={e} />
                    })
                  }
                </div>
                {
                  listMoviesbyType.length > 4 && <div className='btnMovies'>
                    <NavLink className='d-inline-block text-decoration-none border-0 border-rad-8' to={`lich-chieu#${typeMovie}`}>
                      <i className="fa-solid fa-arrow-down fa-bounce"></i> Xem thêm</NavLink>
                  </div>
                }
              </>
            }
          </div>
        </div>
      </div>
      <MoreInfo />
      <Footer />
    </div>
  )
}

const mapStateToProps = (state: { rdcAllMovies: {}[] }) => {
  return {
    allMovies: state.rdcAllMovies,
  }
}
const mapDispatchToProp = (dispatch: any) => {
  return {
    fetchLstAllMovie: () => {
      dispatch({
        type: Action.FETCH_LIST_ALL_MOVIES,
      })
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProp)(Home);