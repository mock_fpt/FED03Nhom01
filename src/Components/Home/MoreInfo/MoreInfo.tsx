import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import './MoreInfo.scss'
import ApiBlog from "./fakeApiBlog.json";   //Fake API Blog
import ApiPromotion from "./fakeApiPromotion.json";   //Fake API Promotion

function MoreInfo() {
    const dataFeatured = ApiBlog.pageProps.dataBlog.Data.ListBlogFeatured.Items;
    const dataFTopView = ApiBlog.pageProps.dataBlog.Data.ListBlogTopView.Items;
    const promotion = ApiPromotion.pageProps.listItems.Data.Items;
    const [[featured, topView, dataBlog], setType] = useState(['active', '', dataFTopView]);

    return (
        <div className='more-info'>
            <div className="blog-preview d-flex justify-content-center">
                <div className="blogSize my-40">
                    <div className="">
                        <h3 className='fs-30 pb-15 text-center'>Blog Điện Ảnh</h3>
                        <p className='fs-20 text-center'>Tổng hợp và Review các bộ phim hot, bom tấn, phim chiếu rạp hay mỗi ngày.</p>
                        <div className="nav my-10 d-flex justify-content-center align-items-center">
                            <button className={`border-0 mx-5 px-15 py-5 fs-18 fw-bolder ${featured}`} onClick={() => setType(['active', '', dataFTopView])}>Mới nhất</button>
                            <button className={`border-0 mx-5 px-15 py-5 fs-18 fw-bolder ${topView}`} onClick={() => setType(['', 'active', dataFeatured])}>Xem nhiều</button>
                        </div>
                    </div>
                    <div className="list-blog d-flex flex-wrap">
                        {
                            dataBlog.map((e, i) => {
                                return <div key={i} className="item-blog w-25 p-10 border-box d-flex flex-col flex-wrap ">
                                    <img className='w-100 mb-5' src={`${e.Avatar}`} alt="banner" />
                                    <p className='mb-10'>{e.Title}</p>
                                    <p className='fs-12'>{e.TotalViewsFormat} lượt xem</p>
                                </div>
                            })
                        }
                    </div>
                    <div className="btnMore text-center border-box"><Link to={'/blog-dien-anh'}><i className="fa-solid fa-arrow-down fa-bounce"></i> Xem thêm</Link></div>
                </div>
            </div>

            <div className="promotion d-flex justify-content-center">
                <div className="promotionSize my-40">
                    <h3 className='fs-30 pb-15 text-center'>Tin tức - Khuyến mãi</h3>
                    <div className="list-promotion d-flex flex-wrap">
                        {
                            promotion.map((e, i) => {
                                return <div key={i} className="item-promotion w-25 p-10 border-box">
                                    <div className="border-box h-100 d-flex flex-col pb-5">
                                        <img className='w-100' src={`${e.Avatar}`} alt="banner" />
                                        <p className='mt-5 mb-10 px-10 fs-16'>{e.Title}</p>
                                        <p className='my-5 text-end px-10'>{e.DateShowFormat}</p>
                                    </div>
                                </div>
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MoreInfo