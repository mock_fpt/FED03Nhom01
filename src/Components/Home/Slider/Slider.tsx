import { eMovieInfo } from 'Components/Interface/eMovieInfo';
import ModalTrailer from 'Components/UI/ModalTrailer/ModalTrailer';
import React, {useState} from 'react'

import { Slide } from 'react-slideshow-image';

import 'react-slideshow-image/dist/styles.css'
import './Slider.scss';

const properties = {
    prevArrow: <button className='prevArrow'><i className="fa-solid fa-angle-left"></i></button>,
    nextArrow: <button className='nextArrow'><i className="fa-solid fa-angle-right"></i></button>
}

export default function Slider(props: any) {
    const [isOpenModal, setIsOpenModal] = useState(false);
    const [currentFilm, setCurrentFilm] = useState<eMovieInfo>({
        'id': "",
        'slug': "",
        'name': "",
        'subName': "",
        'description': "",
        'shortDescription': "",
        'trailer': "",
        'age': "",
        'duration': "",
        'views': 0,
        'startdate': "",
        'enddate': "",
        'imageLandscape': "",
        'imagePortrait': "",
        'point': 0,
        'totalVotes': 0
    });

    return (
        <div className='slide-container'>
            <Slide {...properties}>
                {props.films.map((film : any, index : number)=> (
                <div className="each-slide" key={index}>
                    <div className='d-flex justify-content-center align-items-center' style={{'backgroundImage': `url(${film.imageLandscapeMobile})`}}>
                        <div onClick={() => {
                            setCurrentFilm(film);
                            setIsOpenModal(true);
                        }}><i className="fa-regular fa-circle-play position-absolute"></i>
                        </div>
                    </div>
                </div>
                ))} 
            </Slide>
            {
                isOpenModal && <ModalTrailer setOpenFunc={setIsOpenModal} infoMovie={currentFilm} />
            }
        </div>
    )
}
