import React from 'react'
import { useNavigate } from 'react-router-dom';

import './Introduce.scss';

export default function Introduce() {

    const navigate = useNavigate();


    return (
        <div className='introduce'>
            <div className='d-flex justify-content-center'>
                <div className='mainSize d-flex justify-content-between align-items-center introduce-container border-box py-30'>
                    <div className='introduce-content mr-20'>
                        <h1 className='mb-20'>Đặt mua vé xem phim MoMo</h1>
                        <ol>
                            <li>Du hí các rạp, <span>trải nghiệm phim hay</span></li>
                            <li><span>Đặt vé trực tiếp</span>, thanh toán xong ngay</li>
                            <li>Gợi ý cho bạn, <span>suất chiếu gần đây</span></li>
                            <li><span>Lịch sử đặt vé</span> được lưu lại ngay</li>
                        </ol>
                        <button className='border-box fs-14 text-uppercase border-rad-8 py-10 px-20' onClick={() => navigate("/cinema")}>Đặt vé ngay</button>
                    </div>
                    <div className='introduce-image' style={{ flex: '1 1 0' }}>
                        <img className='w-100 border-box' src="https://static.mservice.io/img/momo-upload-api-210820191534-637650837346813468.jpg" alt="banner promotion" />
                    </div>
                </div>
            </div>
        </div>
    )
}
