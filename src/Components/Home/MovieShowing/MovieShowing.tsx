import React from 'react'

import './MovieShowing.scss';

export default function MovieShowing() {
  return (
    <div className='movie-showing d-flex justify-content-center'>
        <div className='mainSize'>
            <h2>Trailer mới nhất</h2>
            <div className='film-list'>
                <div className='film-item'>
                  <div className='image'>
                    <img src="https://static.mservice.io/cinema/momo-cdn-api-220615142933-637909001734367129.jpg" alt='Film Logo' />
                  </div>
                  <h4>Mèo Đi Hia: Điều Ước Cuối Cùng</h4>
                  <p>Mèo đi hia</p>
                </div>
                <div className='film-item'>
                  <div className='image'>
                    <img src="https://static.mservice.io/cinema/momo-cdn-api-220615142933-637909001734367129.jpg" alt='Film Logo' />
                  </div>
                  <h4>Mèo Đi Hia: Điều Ước Cuối Cùng</h4>
                  <p>Mèo đi hia</p>
                </div>
                <div className='film-item'>
                  <div className='image'>
                    <img src="https://static.mservice.io/cinema/momo-cdn-api-220615142933-637909001734367129.jpg" alt='Film Logo' />
                  </div>
                  <h4>Mèo Đi Hia: Điều Ước Cuối Cùng</h4>
                  <p>Mèo đi hia</p>
                </div>
            </div>
        </div>
    </div>
  )
}
