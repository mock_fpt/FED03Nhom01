# Custom Style CSS

- [Custom Style CSS](#custom-style-css)
  - [BOX SIZING](#box-sizing)
  - [SIZE: WIDTH - HEIGHT](#size-width---height)
  - [PADDING](#padding)
  - [MARGIN](#margin)
  - [TEXT](#text)
    - [Text Alignment](#text-alignment)
    - [Font Size (fs-)](#font-size-fs-)
    - [Font Weight (fw-)](#font-weight-fw-)
    - [Font Style (fst-)](#font-style-fst-)
    - [Text Decoration](#text-decoration)
    - [Text Transform](#text-transform)
  - [POSITION](#position)
    - [Position value](#position-value)
  - [OVERFLOW](#overflow)
  - [FLOAT](#float)
  - [BORDER](#border)
  - [DISPLAY](#display)
    - [Display: Flex](#display-flex)
      - [1. Parent Node](#1-parent-node)
        - [Flex Direction](#flex-direction)
        - [Flex Wrap](#flex-wrap)
        - [Justify Content](#justify-content)
        - [Align Items](#align-items)
      - [2. Child Node](#2-child-node)
        - [Flex Grow](#flex-grow)

## BOX SIZING

---

| Description | Syntax        |
| ----------- | ------------- |
| border-box  | `border-box`  |
| content-box | `content-box` |

## SIZE: WIDTH - HEIGHT

---

| Description    | Syntax        | _{value}_                      | Ex          |
| -------------- | ------------- | ------------------------------ | ----------- |
| Set Width (%)  | `w-`_{value}_ | 10, 20, 25, 30, 50, 100, 100vw | w-10, w-50  |
| Set Height (%) | `h-`_{value}_ | 25, 30, 50, 100, 100vh         | h-25, h-100 |

## PADDING

---

> {value} px: 5, 10, 15, 20, 30, 40

| Description of Padding      | Syntax         | Ex    |
| --------------------------- | -------------- | ----- |
| Top & Bottom & Left & Right | `p-`_{value}_  | p-30  |
| Top & Bottom                | `py-`_{value}_ | py-10 |
| Left & Right                | `px-`_{value}_ | px-5  |
| Top                         | `pt-`_{value}_ | pt-5  |
| Right                       | `pr-`_{value}_ | pr-5  |
| Bottom                      | `pb-`_{value}_ | pb-5  |
| Left                        | `pl-`_{value}_ | pl-5  |

## MARGIN

---

> {value} px: 5, 10, 15, 20, 30, 40

| Description of Margin       | Syntax         | Ex    |
| --------------------------- | -------------- | ----- |
| Top & Bottom & Left & Right | `m-`_{value}_  | m-40  |
| Top & Bottom                | `my-`_{value}_ | my-10 |
| Left & Right                | `mx-`_{value}_ | mx-5  |
| Top                         | `mt-`_{value}_ | mt-5  |
| Right                       | `mr-`_{value}_ | mr-5  |
| Bottom                      | `mb-`_{value}_ | mb-5  |
| Left                        | `ml-`_{value}_ | ml-5  |

## TEXT

---

### Text Alignment

| Description | Syntax         |
| ----------- | -------------- |
| Start       | `text-start`   |
| Center      | `text-center`  |
| End         | `text-end`     |
| Justify     | `text-justify` |

### Font Size (fs-)

`fs-{value}` _{value}_: 12, 14, 20, 25, 45, 64.

### Font Weight (fw-)

`fw-{value}` _{value}_: bolder, bold

### Font Style (fst-)

`fst-italic`

### Text Decoration

`text-decoration-none`

### Text Transform

| Description | Syntax            |
| ----------- | ----------------- |
| Uppercase   | `text-uppercase`  |
| Lowercase   | `text-lowercase`  |
| Capitalize  | `text-capitalize` |

## POSITION

---

### Position value

- `position-relative`
- `position-absolute`
- `position-fixed`

## OVERFLOW

---

- `overflow-auto`
- `overflow-scroll`
- `overflow-scroll-x`
- `overflow-scroll-y`

## FLOAT

---

- `float-start`
- `float-end`

## BORDER

---

| Description of Border | Syntax                       |
| --------------------- | ---------------------------- |
| Border none           | `border-0`                   |
| Border Radius         | `border-rad-`_{value}_       |
|                       | _{value}_: 8, 15, 20, circle |

## DISPLAY

---

| Description of Display | Syntax           |
| ---------------------- | ---------------- |
| Inline                 | `d-inline`       |
| Block                  | `d-block`        |
| Inline-Block           | `d-inline-block` |
| Flex                   | `d-flex`         |

### Display: Flex

#### 1. Parent Node

##### Flex Direction

| Description     | Syntax     |
| --------------- | ---------- |
| Row (_default_) | `flex-row` |
| Column          | `flex-col` |

##### Flex Wrap

| Description | Syntax      |
| ----------- | ----------- |
| Wrap        | `flex-wrap` |

##### Justify Content

| Description       | Syntax                    |
| ----------------- | ------------------------- |
| Start (_default_) | `justify-content-start`   |
| Center            | `justify-content-center`  |
| End               | `justify-content-end`     |
| Space between     | `justify-content-between` |
| Space around      | `justify-content-around`  |

##### Align Items

| Description       | Syntax                |
| ----------------- | --------------------- |
| Start (_default_) | `align-items-start`   |
| Center            | `align-items-center`  |
| End               | `align-items-end`     |
| Stretch           | `align-items-stretch` |

#### 2. Child Node

##### Flex Grow

- `flex-grow-1`
- `flex-grow-2`
- `flex-grow-3`
- `flex-grow-4`
